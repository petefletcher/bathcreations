const express = require("express");
const cors = require("cors");
const bodyParser = require("body-parser");
const path = require("path");
const paypal = require("paypal-rest-sdk");

if (process.env.NODE_ENV !== "production") {
  const dot = require("dotenv").config();
}

const stripe = require("stripe")(process.env.STRIPE_SECRET_KEY);

paypal.configure({
  mode: "sandbox", //sandbox or live
  client_id: process.env.PAYPAL_CLIENT_ID,
  client_secret: process.env.PAYPAL_CLIENT_SECRET,
});

const app = express();
const port = process.env.PORT || 3000;

// any requests get the body and parse to json
app.use(bodyParser.json());
// make sure our urls are in the correct format
app.use(bodyParser.urlencoded({ extended: true }));
// allow requests that come from outside the host - ensures we can make
// requests from port 3000 (client) to port 5000 (server)
app.use(cors());

if (process.env.NODE_ENV === "production") {
  app.use(express.static(path.join(__dirname, "client/dist")));

  app.get("*", function (req, res) {
    // for any route that is not specifically defined
    res.sendFile(path.join(__dirname, "client/dist", "index.html"));
  });
}

app.listen(port, (error) => {
  if (error) throw error;
  console.log("Server running on " + port);
});

app.post("/payment", (req, res) => {
  const { items } = req.body;
  const body = {
    source: req.body.token.id,
    amount: req.body.amount,
    currency: "gbp",
  };

  stripe.charges.create(body, async (stripeError, stripeRes) => {
    if (stripeError) {
      res.status(500).send({ error: stripeError });
    } else {
      const { source, billing_details, currency, amount } = stripeRes;
      let decimalTotal = amount / 100;

      let transactionData = {
        items_list: {
          items: [...items],
          shipping_address: billing_details.address,
        },
        payee: { name: source.name, merchant_id: source.id },
        amount: { currency: currency, total: decimalTotal.toFixed(2) },
      };
      res.status(200).send({ success: transactionData, items: items });
    }
  });
});

app.post("/payments", (req, res) => {
  const { cartItems, cartAmount, items } = req.body;

  const formatCartItems = [];
  cartItems.forEach((item) => {
    delete item.category;
    delete item.createdAt;
    delete item.id;
    delete item.imageUrl;
    delete item.link;
    delete item.size;
    delete item.tag;
    formatCartItems.push(item);
  });

  const create_payment_json = {
    intent: "sale",
    payer: {
      payment_method: "paypal",
    },
    redirect_urls: {
      // TODO needs changing for production
      return_url: "http://localhost:3000/success",
      cancel_url: "http://localhost:8080",
    },
    transactions: [
      {
        item_list: {
          items: [...formatCartItems],
        },
        amount: { ...cartAmount },
        description: `You have purchased`,
      },
    ],
  };

  paypal.payment.create(create_payment_json, function (error, payment) {
    if (error) {
      console.log(error.response.details);
    } else {
      const { links } = payment;
      links.forEach((link) => {
        if (link.rel === "approval_url") {
          res.send(link.href);
        }
      });
    }
  });
});

app.get("/success", (req, res) => {
  const payerId = req.query.PayerID;
  const paymentId = req.query.paymentId;

  const execute_payment_json = {
    payer_id: payerId,
  };

  paypal.payment.execute(paymentId, execute_payment_json, function (
    error,
    payment
  ) {
    let paymentIdSuccess = "";
    if (error) {
      console.log(error.response);
      res.send(error.response);
    } else {
      paymentIdSuccess = payment.id;
      res.redirect(`http://localhost:8080/checkout-complete?${paymentId}`);
    }
  });
});

app.get("/transactions", (req, res) => {
  const paymentId = Object.keys(req.query)[0];
  paypal.payment.get(paymentId, function (error, payment) {
    if (error) {
      alert("Something went wrong with after the payment");
      throw error;
    } else {
      const transactionDetails = {
        amount: payment.transactions[0].amount,
        payee: [
          payment.payer.payer_info.first_name,
          payment.transactions[0].payee.email,
        ],
        items: payment.transactions[0].item_list,
      };

      res.send(transactionDetails);
    }
  });
});
