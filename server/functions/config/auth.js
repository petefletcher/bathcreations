const admin = require("./adminConfig");

exports.checkAuthToken = async (req, res, next) => {
  let idToken;

  if (
    req.headers.authorization &&
    req.headers.authorization.startsWith("Bearer ")
  ) {
    /** we need to split the header now */
    idToken = req.headers.authorization.split("Bearer ")[1];
  } else {
    return res.status(403).json({ error: "Unauthorised" });
  }

  try {
    const decodedToken = await admin.auth().verifyIdToken(idToken);

    res.user = decodedToken;

    const getUserDataFromTokenRef = await admin.firestore().collection("users");
    const docs = await getUserDataFromTokenRef
      .where("userId", "==", res.user.uid)
      .get();

    res.user.email = decodedToken.email;

    return next();
  } catch (error) {
    console.log(error);
    return res
      .status(403)
      .json({ error: { message: "there was an auth problem" } });
  }
};

/** Check user is admin */
exports.isAdmin = (req, res, next) => {
  let idToken;
  idToken = req.headers.authorization.split("Bearer ")[1];

  // Verify the ID token first.
  admin
    .auth()
    .verifyIdToken(idToken)
    .then((user) => {
      if (user.admin === true) {
        return next();
      } else {
        return res.status(403).json({ error: "user not admin" });
      }
    });
};
