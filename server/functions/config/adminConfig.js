const admin = require("firebase-admin");
const serviceAccount = require("./bath-creations.json");
// const serviceAccount = require("../../../../GCloud/bath-creations.json");
const fbConfig = require("./firebaseConfig");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  storageBucket: fbConfig.firebaseConfig.storageBucket,
});

module.exports = admin;
