const functions = require("firebase-functions");
const bodyParser = require("body-parser");
const express = require("express");
const path = require("path");

const app = express();
const cors = require("cors");

const { checkAuthToken, isAdmin } = require("./config/auth");
const { setAdminRole } = require("./config/createAdmin");
const {
  getProducts,
  addProduct,
  editProduct,
  deleteProduct,
  uploadImage,
} = require("./controllers/products");
const {
  getCategories,
  addCategory,
  editCategory,
  deleteCategory,
  uploadCategoryImage,
} = require("./controllers/categories");
const { userLogin, verifyAdmin, userSession } = require("./controllers/users");
const {
  paypalPayments,
  paypalSuccess,
  paypalTransaction,
  stripePayment,
  getTransaction,
  allTransactions,
} = require("./controllers/payments");

app.use(cors({ origin: true }));
app.use((req, res, next) => {
  res.set("Access-Control-Allow-Origin", "*");
  res.set(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept, Authorization"
  );
  res.set("Access-Control-Allow-Methods", "PUT, POST, PATCH, DELETE, GET");
  console.log("RES headder", res.headersSent);
  if (req.method === "OPTIONS") {
    console.log("OPTIONs", res);
    return res.status(200).json({});
  }
  next();
});

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

/** Products */
app.get("/v1/products", getProducts);
app.post("/v1/products", checkAuthToken, isAdmin, addProduct);
app.post(
  "/v1/products/upload-product-image",
  checkAuthToken,
  isAdmin,
  uploadImage
);
app.put("/v1/products/edit-product", checkAuthToken, isAdmin, editProduct);
app.delete(
  `/v1/products/remove-product`,
  checkAuthToken,
  isAdmin,
  deleteProduct
);

/** Categories */
app.get("/v1/categories", getCategories);
app.post("/v1/categories", checkAuthToken, isAdmin, addCategory);
app.post(
  "/v1/categories/upload-category-image",
  checkAuthToken,
  isAdmin,
  uploadCategoryImage
);
app.put("/v1/categories/edit-category", checkAuthToken, isAdmin, editCategory);
app.delete(
  `/v1/categories/remove-category`,
  checkAuthToken,
  isAdmin,
  deleteCategory
);

/** Admin Login */
app.post("/v1/admin-login", userLogin);
app.post("/v1/verify-admin", isAdmin, verifyAdmin);
app.post("/v1/set-admin", setAdminRole);
app.post("/v1/user-session", userSession);

/** Payments */
app.post("/v1/payments", paypalPayments);
app.get("/v1/success", paypalSuccess);
app.get("/v1/transactions", paypalTransaction);
app.post("/v1/stripePayment", stripePayment);
app.get("/v1/get-transaction", getTransaction);
app.get("/v1/all-transactions", checkAuthToken, isAdmin, allTransactions);

exports.app = functions.region("europe-west2").https.onRequest(app);
