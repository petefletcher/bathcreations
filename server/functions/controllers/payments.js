const path = require("path");
const paypal = require("paypal-rest-sdk");
const admin = require("../config/adminConfig");
const { paymentEnvVars } = require("../config/envVars");
const { getIndividual, getAll } = require("../utils/dbCrudCalls");

const stripe = require("stripe")(paymentEnvVars.STRIPE_SECRET_KEY);

console.log("ENVIRONMENT", process.env.FUNCTIONS_EMULATOR);

let isTest;

if (process.env.FUNCTIONS_EMULATOR) {
  isTest = true;
} else {
  isTest = false;
}

paypal.configure({
  mode: "sandbox", //sandbox or live
  client_id: paymentEnvVars.PAYPAL_CLIENT_ID,
  client_secret: paymentEnvVars.PAYPAL_CLIENT_SECRET,
});

exports.paypalPayments = (req, res) => {
  const { cartItems, cartAmount } = req.body;

  const formatCartItems = [];
  cartItems.forEach((item) => {
    delete item.category;
    delete item.createdAt;
    delete item.id;
    delete item.imageUrl;
    delete item.link;
    delete item.size;
    delete item.tag;
    formatCartItems.push(item);
  });

  const create_payment_json = {
    intent: "sale",
    payer: {
      payment_method: "paypal",
    },
    redirect_urls: {
      return_url: isTest
        ? "http://localhost:5001/bath-creations/europe-west2/app/v1/success"
        : "https://europe-west2-bath-creations.cloudfunctions.net/app/v1/success",
      cancel_url: isTest
        ? "http://localhost:8080"
        : "https://bath-creations.firebaseapp.com/",
    },
    transactions: [
      {
        item_list: {
          items: [...formatCartItems],
        },
        amount: { ...cartAmount },
        description: `You have purchased`,
      },
    ],
  };

  paypal.payment.create(create_payment_json, function (error, payment) {
    if (error) {
    } else {
      const { links } = payment;
      links.forEach((link) => {
        if (link.rel === "approval_url") {
          res.send(link.href);
        }
      });
    }
  });
};

exports.paypalSuccess = (req, res) => {
  const payerId = req.query.PayerID;
  const paymentId = req.query.paymentId;

  const execute_payment_json = {
    payer_id: payerId,
  };

  paypal.payment.execute(paymentId, execute_payment_json, function (
    error,
    payment
  ) {
    let paymentIdSuccess = "";
    if (error) {
      res.send(error.response);
    } else {
      paymentIdSuccess = payment.id;
      isTest
        ? res.redirect(`http://localhost:8080/checkout-complete?${paymentId}`)
        : res.redirect(
            `https://bath-creations.firebaseapp.com/checkout-complete?${paymentId}`
          );
    }
  });
};

exports.paypalTransaction = (req, res) => {
  const paymentId = Object.keys(req.query)[0];
  paypal.payment.get(paymentId, async function (error, payment) {
    if (error) {
      alert("Something went wrong with after the payment");
      throw error;
    } else {
      const transactionDetails = {
        amount: payment.transactions[0].amount,
        payee: [
          payment.payer.payer_info.first_name,
          payment.transactions[0].payee.email,
        ],
        items: payment.transactions[0].item_list,
        createdAt: new Date(),
      };

      try {
        delete payment.transactions[0].related_resources;
        const transactionRef = await admin
          .firestore()
          .collection("transactions")
          .doc(`${payment.transactions[0]}`);
        const doc = await transactionRef.get();

        if (!doc.exists) {
          const resp = await admin
            .firestore()
            .collection("transactions")
            .doc(`${payment.id}`)
            .set(payment.transactions[0]);

          return res.send(transactionDetails);
        } else {
          return res.status(400).json({
            message: "Something went wrong -- This transaction already exists",
          });
        }
      } catch (error) {
        res.send({
          message: "Sorry, something went wrong -- product not added",
          error,
        });
      }
    }
  });
};

exports.stripePayment = (req, res) => {
  const { items } = req.body;
  const body = {
    source: req.body.token.id,
    amount: req.body.amount,
    currency: "gbp",
  };

  stripe.charges.create(body, async (stripeError, stripeRes) => {
    if (stripeError) {
      res.status(500).send({ error: stripeError });
    } else {
      const { source, billing_details, currency, amount } = stripeRes;
      let decimalTotal = amount / 100;

      let transactionData = {
        item_list: {
          items: [...items],
          shipping_address: billing_details.address,
        },
        payee: { name: source.name, merchant_id: source.id },
        amount: { currency: currency, total: decimalTotal.toFixed(2) },
        createdAt: new Date(),
      };
      try {
        const transactionRef = await admin
          .firestore()
          .collection("transactions")
          .doc(`${transactionData}`);
        const doc = await transactionRef.get();
        if (!doc.exists) {
          const resp = await admin
            .firestore()
            .collection("transactions")
            .doc(`${source.id}`)
            .set(transactionData);

          res.status(200).send({ success: transactionData, items: items });
        } else {
          return res.status(400).json({
            message: "Something went wrong -- This transaction already exists",
          });
        }
      } catch (error) {
        res.send({
          message: "Sorry, something went wrong -- product not added",
          error,
        });
      }
    }
  });
};

exports.getTransaction = async (req, res) => {
  const { id } = req.query;
  try {
    const transResult = await getIndividual(id, "transactions");
    res.send(transResult);
  } catch (error) {
    res.send({
      message: "Sorry, something went wrong -- transaction not available",
      error,
    });
  }
};

exports.allTransactions = (req, res) => {
  getAll(req, res);
};
