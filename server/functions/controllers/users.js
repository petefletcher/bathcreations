const admin = require("../config/adminConfig");
const { firebaseConfig } = require("../config/firebaseConfig");
const firebase = require("firebase");

// Initialize Firebase
firebase.initializeApp(firebaseConfig);

exports.userLogin = async (req, res) => {
  let token;
  const userlogin = {
    email: req.body.email,
    password: req.body.password,
  };

  try {
    const data = await firebase
      .auth()
      .signInWithEmailAndPassword(userlogin.email, userlogin.password);

    const userToken = await data.user.getIdToken();
    token = userToken;
    return res.json({ token });
  } catch (error) {
    switch (error.code) {
      case "auth/invalid-email":
        return res.status(400).json({ error: "Please enter valid email" });
        break;
      case "auth/user-not-found":
        return res.status(404).json({ error: "User not found" });
        break;
      case "auth/wrong-password":
        return res.status(401).json({ error: "Invalid password" });
        break;

      default:
    }
  }
};

exports.userSession = async (req, res) => {
  if (!req.body.token) return res.json({ message: "No token suppied" });
  const { token } = req.body;

  try {
    const decodedToken = await admin.auth().verifyIdToken(token);
    const currentSession = {
      uid: decodedToken.uid,
      email: decodedToken.email,
      expiry: decodedToken.exp,
    };
    res.send({ currentSession, token });
  } catch (error) {
    res
      .status(400)
      .send({ message: "Sorry unable to create the session", error });
  }
};

exports.verifyAdmin = async (req, res) => {
  //user logs in with their credentials and verifies their token.
  const { email } = req.body;
  let idToken = req.headers.authorization.split("Bearer ")[1];

  try {
    const userRef = await admin.auth().getUserByEmail(email);

    const getClaims = await admin.auth().verifyIdToken(idToken);

    if (getClaims.admin === true) {
      return res.json({
        message: `${getClaims.email} is a verified admin`,
        claim: getClaims.admin,
      });
    } else {
      return res.json({ message: `${getClaims.email} is not admin` });
    }
  } catch (error) {
    return res.json(error);
  }
};
