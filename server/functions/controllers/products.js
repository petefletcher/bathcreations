const admin = require("../config/adminConfig");
const fbConfig = require("../config/firebaseConfig");
const cors = require("cors");
const { v4: uuidv4 } = require("uuid");
const { imageUploader } = require("../utils/imageUploader");
const { getAll, deleteItem } = require("../utils/dbCrudCalls");

exports.getProducts = async (req, res) => {
  getAll(req, res);
};

exports.addProduct = (req, res) => {
  const { type } = req.query;

  const BusBoy = require("busboy");
  const path = require("path");
  const os = require("os");
  const fs = require("fs");

  const uuid = uuidv4();
  console.log("reqheaders", req.headers);
  const busboy = new BusBoy({ headers: req.headers });
  const tmpdir = os.tmpdir();

  let imageFileName;
  // This object will accumulate all the uploaded files, keyed by their name.
  let imageToBeUploaded = {};
  const fields = {};

  busboy.on("field", (fieldname, val) => {
    console.log("FIELDS", fieldname);
    fields[fieldname] = val;
  });
  const fileWrites = [];

  busboy.on("file", (fieldname, file, filename, encoding, mimetype) => {
    if (mimetype !== "image/jpeg" && mimetype !== "image/png") {
      return res.status(400).json({ message: "wrong file type submitted" });
    }
    // image.png
    const imageExtension = filename.split(".").pop();
    //random number.png
    imageFileName = `${Math.round(Math.random() * 10000000)}.${imageExtension}`;
    const filePath = path.join(tmpdir, imageFileName);
    imageToBeUploaded[fieldname] = filePath;
    imageToBeUploaded["type"] = mimetype;

    const writeStream = fs.createWriteStream(filePath);
    console.log("DEBUG WRITE STREAM", imageToBeUploaded);
    file.pipe(writeStream);
    fileWrites.push(imageToBeUploaded);
  });

  busboy.on("finish", async () => {
    let url = `https://firebasestorage.googleapis.com/v0/b/${fbConfig.firebaseConfig.storageBucket}/o/${imageFileName}?alt=media`;
    let docAdded = false;

    fileWrites.forEach(async (file) => {
      const { category, name, price, size, tag, link } = fields;

      const newProduct = {
        category,
        currency: "GBP", // added automatically
        name,
        price,
        size,
        tag,
        imageUrl: url,
        link,
        createdAt: new Date().toISOString(), // added automatically
      };
      console.log("FILeS");
      /** Add Item */
      try {
        console.log("productsRef");

        const productsRef = await admin
          .firestore()
          .collection(`${type}`)
          .doc(`${newProduct.link}`);

        const doc = await productsRef.get();
        if (!doc.exists) {
          const resp = await admin
            .firestore()
            .collection("products")
            .doc(`${newProduct.link}`)
            .set(newProduct);

          docAdded = true;
          res.send({ message: "Item was successfully added" });
        } else {
          return res
            .status(400)
            .json({ message: "This product already exists" });
        }
      } catch (error) {
        return res.send({
          message: "Something went wrong when trying to add the product",
          error,
        });
      }

      if (fileWrites.length > 1) {
        res.send({ message: "Too many files selected" });
      } else {
        if (docAdded) {
          /** Add files to storage */
          try {
            await admin
              .storage()
              .bucket()
              .upload(file.image, {
                resumable: false,
                metadata: {
                  metadata: {
                    contentType: file.type,
                    // needed this below to create access token
                    firebaseStorageDownloadTokens: uuid,
                  },
                },
              });
          } catch (error) {
            return res.send({
              message: "Something went wrong with the upload",
              error,
            });
          }
        } else {
          res.send({ message: "Document did not add correctly" });
        }
      }
    });
  });
  busboy.end(req.rawBody);
};

exports.editProduct = async (req, res) => {
  const {
    category,
    name,
    price,
    size,
    tag,
    imageUrl,
    link,
  } = req.body.productData;

  const editedProduct = {
    category,
    name,
    price,
    size,
    tag,
    imageUrl,
    link,
  };

  try {
    const productsRef = await admin
      .firestore()
      .collection("products")
      .doc(`${link}`);

    const doc = await productsRef.get();

    if (!doc.exists) {
      return res.status(400).json({ message: "This product does not exist" });
    } else {
      const resp = await admin
        .firestore()
        .collection("products")
        .doc(`${editedProduct.link}`)
        .set(editedProduct);

      return res
        .status(201)
        .json({ message: "New product successfully edited", editedProduct });
    }
  } catch (error) {
    res.send({
      message: "Sorry, something went wrong -- product not added",
      error,
    });
  }
};

exports.deleteProduct = async (req, res) => {
  deleteItem(req, res);
};

exports.uploadImage = (req, res) => {
  imageUploader(req, res);
};
