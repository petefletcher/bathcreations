const admin = require("../config/adminConfig");
const fbConfig = require("../config/firebaseConfig");
const { imageUploader } = require("../utils/imageUploader");
const { getAll, deleteItem } = require("../utils/dbCrudCalls");

exports.getCategories = async (req, res) => {
  getAll(req, res);
};

exports.deleteCategory = async (req, res) => {
  deleteItem(req, res);
};

exports.uploadCategoryImage = (req, res) => {
  imageUploader(req, res);
};

exports.editCategory = async (req, res) => {
  const { link, category } = req.body.categoryData;
  const editedCategory = {
    category,
    link,
  };

  try {
    const categoriesRef = await admin
      .firestore()
      .collection("categories")
      .doc(`${link}`);

    const doc = await categoriesRef.get();

    if (!doc.exists) {
      return res.status(400).json({ message: "This category does not exist" });
    } else {
      const resp = await admin
        .firestore()
        .collection("categories")
        .doc(`${editedCategory.link}`)
        .set(editedCategory);

      return res
        .status(201)
        .json({ message: "Product successfully edited", editedCategory });
    }
  } catch (error) {
    res.send({
      message: "Sorry, something went wrong -- category not added",
      error,
    });
  }
};

exports.addCategory = async (req, res) => {
  const { category, link } = req.body.categoryData;

  const newCategory = {
    category,
    link,
    createdAt: new Date().toISOString(), // added automatically
  };

  try {
    const categoriesRef = await admin
      .firestore()
      .collection("categories")
      .doc(`${newCategory.link}`);

    const doc = await categoriesRef.get();

    if (!doc.exists) {
      const resp = await admin
        .firestore()
        .collection("categories")
        .doc(`${newCategory.link}`)
        .set(newCategory);

      return res
        .status(201)
        .json({ message: "New Category successfully added", newCategory });
    } else {
      return res.status(400).send({ message: "This Category already exists" });
    }
  } catch (error) {
    console.log(error);
    res.status(400).send({
      message: "Sorry, something went wrong -- Category not added",
      error,
    });
  }
};
