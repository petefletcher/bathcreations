const admin = require("../config/adminConfig");

exports.getAll = async (req, res) => {
  const { type } = req.query;
  let items = [];

  try {
    const itemsRef = await admin.firestore().collection(`${type}`);

    const allitems = await itemsRef.get();

    if (allitems.size === 0) {
      res.send({
        message: `This category does not exist or there are no items available`,
      });
    } else {
      allitems.forEach((doc) => {
        items.push({ id: doc.id, ...doc.data() });
      });

      return res.json(items);
    }
  } catch (error) {
    res.send({
      message: `Sorry, something went wrong -- could find any ${type}`,
      error,
    });
  }
};

exports.deleteItem = async (req, res) => {
  const { type, item } = req.query;

  try {
    const productRef = await admin.firestore().collection(type).doc(`${item}`);

    const doc = await productRef.get();

    if (!doc.exists) {
      return res.status(400).json({ message: "This product does not exists" });
    } else {
      const resp = await admin
        .firestore()
        .collection(type)
        .doc(`${item}`)
        .delete();

      return res
        .status(201)
        .json({ message: "Product successfully deleted", item });
    }
  } catch (error) {
    res.send({
      message: "Sorry, something went wrong -- could not delete the product",
      error,
    });
  }
};

exports.getIndividual = async (id, context) => {
  try {
    const itemRef = await admin.firestore().collection(context).doc(id);
    const doc = await itemRef.get();
    if (!doc.exists) {
      return { error: "Transaction does not exist" };
    } else {
      return doc.data();
    }
  } catch (error) {
    return error;
  }
};
