const { v4: uuidv4 } = require("uuid");
const admin = require("../config/adminConfig");
const fbConfig = require("../config/firebaseConfig");

exports.imageUploader = (req, res) => {
  const { type, name } = req.query;
  const BusBoy = require("busboy");
  const path = require("path");
  const os = require("os");
  const fs = require("fs");

  const uuid = uuidv4();
  const busboy = new BusBoy({ headers: req.headers });
  const tmpdir = os.tmpdir();

  let imageFileName;
  // This object will accumulate all the uploaded files, keyed by their name.
  let imageToBeUploaded = {};
  const fields = {};

  busboy.on("field", (fieldname, val) => {
    fields[fieldname] = val;
  });

  const fileWrites = [];

  busboy.on("file", (fieldname, file, filename, encoding, mimetype) => {
    if (mimetype !== "image/jpeg" && mimetype !== "image/png") {
      return res.status(400).json({ message: "wrong file type submitted" });
    }
    // image.png
    const imageExtension = filename.split(".").pop();
    //random number.png
    imageFileName = `${Math.round(Math.random() * 10000000)}.${imageExtension}`;
    const filePath = path.join(tmpdir, imageFileName);
    imageToBeUploaded[fieldname] = filePath;
    imageToBeUploaded["type"] = mimetype;

    const writeStream = fs.createWriteStream(filePath);
    file.pipe(writeStream);
    fileWrites.push(imageToBeUploaded);
  });

  busboy.on("finish", async () => {
    let url = `https://firebasestorage.googleapis.com/v0/b/${fbConfig.firebaseConfig.storageBucket}/o/${imageFileName}?alt=media`;
    let docAdded = false;

    fileWrites.forEach(async (file) => {
      const { category, name, price, size, tag, link } = fields;

      const newProduct = {
        category,
        currency: "GBP", // added automatically
        name,
        price,
        size,
        tag,
        imageUrl: url,
        link,
        createdAt: new Date().toISOString(), // added automatically
      };

      /** Add Item */
      try {
        const productsRef = await admin
          .firestore()
          .collection(`${type}`)
          .doc(`${newProduct.link}`);

        const doc = await productsRef.get();
        if (!doc.exists) {
          const resp = await admin
            .firestore()
            .collection("products")
            .doc(`${newProduct.link}`)
            .set(newProduct);

          docAdded = true;
        } else {
          return res
            .status(400)
            .json({ message: "This product already exists" });
        }
      } catch (error) {
        return res.send({
          message: "Something went wrong when trying to add the product",
          error,
        });
      }

      if (fileWrites.length > 1) {
        res.send({ message: "Too many files selected" });
      } else {
        if (docAdded) {
          /** Add files to storage */
          try {
            await admin
              .storage()
              .bucket()
              .upload(file.image, {
                resumable: false,
                metadata: {
                  metadata: {
                    contentType: file.type,
                    // needed this below to create access token
                    firebaseStorageDownloadTokens: uuid,
                  },
                },
              });
          } catch (error) {
            return res.send({
              message: "Something went wrong with the upload",
              error,
            });
          }
        } else {
          res.send({ message: "Document did not add correctly" });
        }
      }
    });
    res.send({ message: "Item was successfully added" });
  });
  busboy.end(req.rawBody);
};
