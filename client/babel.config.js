// babel.config.js
// by default jest set node env to test when it runs
const isTest = String(process.env.NODE_ENV) === "test";

module.exports = {
  presets: [
    [
      "@babel/env",
      {
        modules: isTest ? "commonjs" : false,
      },
    ],
    "@babel/react",
  ],
  // was getting regeneration runtime error
  plugins: ["@babel/plugin-transform-runtime"],
};
