const localStorageMock = {
  getItem: jest.fn(),
  setItem: jest.fn(() => {
    return { bcToken: "qwerty" };
  }),
  removeItem: jest.fn(),
  clear: jest.fn(),
};

global.localStorage = localStorageMock;
