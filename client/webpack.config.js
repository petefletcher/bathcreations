const HtmlWebPackPlugin = require("html-webpack-plugin");
const path = require("path");

module.exports = {
  entry: "./src/index.js",
  output: {
    filename: "bundle.js",
    path: path.resolve(__dirname, "build"),
    publicPath: "/",
  },
  devServer: {
    contentBase: "./build",
    historyApiFallback: {
      index: "/",
    },
    proxy: {
      "/verify-admin": {
        target: "http://localhost:5001/bath-creations/europe-west2/app/v1",
        secure: false,
        changeOrigin: true,
      },
      "/stripePayment": {
        target: "http://localhost:5001/bath-creations/europe-west2/app/v1",
        secure: false,
        changeOrigin: true,
      },
      "/payments": {
        target: "http://localhost:5001/bath-creations/europe-west2/app/v1",
        secure: false,
        changeOrigin: true,
      },
      "/products": {
        target: "http://localhost:5001/bath-creations/europe-west2/app/v1",
        secure: false,
        changeOrigin: true,
      },
    },

    headers: {
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Headers": "*",
      "Access-Control-Allow-Methods": "*",
    },
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader",
        },
      },
      {
        test: /\.html$/,
        use: [
          {
            loader: "html-loader",
          },
        ],
      },
      {
        test: /\.(s*)css$/,
        use: ["style-loader", "css-loader", "sass-loader"],
      },
      {
        test: /\.(png|jpe?g|gif)$/i,
        use: [
          {
            loader: "file-loader",
          },
        ],
      },
    ],
  },
  plugins: [
    new HtmlWebPackPlugin({
      template: "./src/index.html",
      filename: "./index.html",
      favicon: "./src/img/bc-logo.jpg",
    }),
  ],
};
// For every file with a js or jsx extension Webpack pipes the code through babel-loader. With this in place we're ready to write a React.
// loader --> node module that helps compile or transpile --> eg
// --> css loader complies css to string, style-loader then puts this string in a styler tag in the index.js
// used sass-loader for the sass files
// babel loader helps convert new js eg es6+ or jsx into plain js so can be read
