import React, { useState } from "react";

import Form from "./utils/FormInput";
import Button from "./utils/Button";

import { signUpStart } from "../redux/user/user.action";
import { useDispatch } from "react-redux";

import "../styles/SignUp.scss";
import FormInput from "./utils/FormInput";

const SignUp = () => {
  const [newUserDetails, setNewUserDetails] = useState({
    displayName: "",
    email: "",
    password: "",
    confirmPassword: "",
  });

  const dispatch = useDispatch();

  const handleChangeForm = (event) => {
    const { value, id } = event.target;

    setNewUserDetails({
      ...newUserDetails,
      [id]: value,
    });
  };

  const submitForm = async (event) => {
    event.preventDefault();
    //Reset search value

    const { displayName, email, password, confirmPassword } = newUserDetails;

    if (password !== confirmPassword) {
      alert("Passwords dont match");
      return;
    }

    dispatch(signUpStart({ displayName, email, password, confirmPassword }));

    setNewUserDetails({
      displayName: "",
      email: "",
      password: "",
      confirmPassword: "",
    });
  };

  return (
    <div className="sign-up">
      <h2>Sign me up!</h2>
      <span className="sign-up__sub-title">
        Sign up with your email and password
      </span>
      <form className="sign-up__form">
        <FormInput
          name="displayName"
          id="displayName"
          type="text"
          placeholder="Display Name"
          onChange={handleChangeForm}
          value={newUserDetails.displayName}
        />
        <FormInput
          name="email"
          id="email"
          type="email"
          placeholder="Email"
          onChange={handleChangeForm}
          value={newUserDetails.email}
        />
        <FormInput
          name="password"
          id="password"
          type="password"
          placeholder="Password"
          onChange={handleChangeForm}
          value={newUserDetails.password}
        />
        <FormInput
          name="confirmPassword"
          id="confirmPassword"
          type="password"
          placeholder="Confirm Password"
          onChange={handleChangeForm}
          value={newUserDetails.confirmPassword}
        />
      </form>
      <Button id="sign-up" textValue="Sign Up" onClickEvent={submitForm} />
    </div>
  );
};

export default SignUp;
