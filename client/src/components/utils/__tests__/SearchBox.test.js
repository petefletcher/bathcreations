import React from "react";
import "@testing-library/jest-dom/extend-expect";
import { render, fireEvent } from "@testing-library/react";
import SearchBox from "../SearchBox";

describe("Search Box testing", () => {
  const historyMock = { push: jest.fn(), location: {}, listen: jest.fn() };

  it("should have a search bar which is an input, search button", () => {
    const onSubmit = jest.fn();
    const { getByText, getByRole, debug } = render(
      <SearchBox handleSubmit={onSubmit()} open={true} />
    );

    debug();

    expect(getByText("Search")).toBeInTheDocument();
    expect(getByText("Search").tagName).toBe("BUTTON");

    fireEvent.change(getByRole("textbox"), {
      target: { value: "Test" },
    });

    expect(getByRole("textbox")).toHaveValue("Test");
    fireEvent.click(getByText("Search"));
    expect(onSubmit).toBeCalledTimes(1);
    expect(getByRole("textbox").value).toBe("");
  });

  it("SearchBox snapshot", () => {
    const { container } = render(<SearchBox />);
    expect(container.firstChild).toMatchSnapshot();
  });
});
