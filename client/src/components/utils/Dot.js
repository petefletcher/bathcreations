import React from "react";

function Dot({ name, textValue, onClickEvent }) {
  return (
    <div className={name} onClick={(e) => onClickEvent(e)}>
      {textValue}
    </div>
  );
}

export default Dot;
