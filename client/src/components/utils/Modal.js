import React, { useState, useEffect } from "react";
import "../../styles/Modal.scss";
import FormInput from "../utils/FormInput";
import Button from "../utils/Button";
import Aux from "../../hoc/AuxComponent";

import { formValidation } from "../utils/formValidation";
import {
  addProductToCollection,
  addCategoryToCollection,
  editCategoryInCollection,
  editProductInCollection,
  deleteItemFromCollection,
} from "../utils/apiCalls";

const CrudModal = (props) => {
  const [productDetails, setProductDetails] = useState({
    name: "",
    link: "",
    price: "",
    size: "",
    tag: "",
    image: "",
    category: "",
  });
  const [categoryDetails, setCategoryDetails] = useState({
    category: "",
    link: "",
  });

  const [image, setImage] = useState(null);

  const [editCategoryDetails, setEditCategoryDetails] = useState(props.item);
  const [editProductDetails, setEditProductDetails] = useState(props.item);
  const helperText = document.getElementById("helper-text");

  useEffect(() => {
    if (props.crudMethod !== "delete") {
      if (props.type === "product") {
        const button = (document.getElementById(
          "model-add-update-button"
        ).disabled = true);
      } else {
        const button = (document.getElementById(
          "modal-category-button"
        ).disabled = true);
      }
    }
  }, []);

  const handleBlur = (event) => {
    if (formValidation(event, props.type) && props.type === "product") {
      const button = (document.getElementById(
        "model-add-update-button"
      ).disabled = false);
    }

    if (formValidation(event, props.type) && props.type === "category") {
      const button = (document.getElementById(
        "modal-category-button"
      ).disabled = false);
    }
  };

  const handleChangeForm = (event) => {
    const { value, name, files } = event.target;
    if (props.type === "product") {
      // Are we editing?
      if (!props.item) {
        if (files) {
          const image = files[0];
          console.log("file", image);
          setImage(image);
          setProductDetails({
            ...productDetails,
            image,
          });
        }

        setProductDetails({
          ...productDetails,
          [name]: value,
        });
      } else {
        setEditProductDetails({
          ...editProductDetails,
          [name]: value,
        });
      }
    }

    if (props.type === "category") {
      const { value, name } = event.target;
      if (!props.item) {
        setCategoryDetails({
          ...categoryDetails,
          [name]: value,
        });
      } else {
        setEditCategoryDetails({
          ...editCategoryDetails,
          [name]: value,
        });
      }
    }
  };

  const handleSubmit = async () => {
    if (props.type === "product") {
      if (props.crudMethod === "add") {
        const formData = new FormData();

        formData.append("image", image, image.name);
        formData.append("name", productDetails.name);
        formData.append("link", productDetails.link);
        formData.append("price", productDetails.price);
        formData.append("size", productDetails.size);
        formData.append("tag", productDetails.tag);
        formData.append("category", productDetails.category);
        try {
          let result = await addProductToCollection(formData);
          console.log(result);

          if (result && result.message !== undefined) {
            alert(result.message);
            window.location.href = "/manage-products";
          }
        } catch (error) {
          console.log(error);
          alert(error);
        }
      }

      if (props.crudMethod === "edit") {
        try {
          const editProduct = await editProductInCollection(editProductDetails);
          alert(editProduct.message);
          window.location.href = "/manage-products";
        } catch (error) {
          const { message } = error.response.data;
          alert(message);
        }
      }

      if (props.crudMethod === "delete") {
        try {
          const deleteProduct = await deleteItemFromCollection(
            editProductDetails,
            props.type
          );
          alert(deleteProduct.message);
          window.location.href = "/manage-products";
        } catch (error) {
          const { message } = error.response.data;
          alert(message);
        }
      }
      props.clicked();
    }

    if (props.type === "category") {
      if (props.crudMethod === "add") {
        try {
          const addCategory = await addCategoryToCollection(categoryDetails);
          alert(addCategory.message);
          window.location.href = "/manage-categories";
        } catch (error) {
          const { message } = error.response.data;
          alert(message);
        }
      }

      if (props.crudMethod === "edit") {
        try {
          const editCategory = await editCategoryInCollection(
            editCategoryDetails
          );
          alert(editCategory.message);
          window.location.href = "/manage-categories";
        } catch (error) {
          const { message } = error.response.data;
          alert(message);
        }
      }

      if (props.crudMethod === "delete") {
        try {
          const deleteCategory = await deleteItemFromCollection(
            editCategoryDetails,
            props.type
          );
          alert(deleteCategory.message);
          window.location.href = "/manage-categories";
        } catch (error) {
          const { message } = error.response.data;
          alert(message);
        }
      }
      props.clicked();
    }
  };

  const handleImageUpload = () => {
    const fileInput = document.getElementById("productImageUpload");
    console.log();
    fileInput.click();
  };

  if (props.type === "product") {
    return (
      <form
        id="product-form"
        className="modal"
        onSubmit={handleSubmit}
        encType="multipart/form-data"
      >
        <div className="modal__cell">
          <label htmlFor="product-name">Product Name</label>
          <FormInput
            type="text"
            id="modal-product-name"
            name="name"
            placeholder="eg Strawberry"
            onChange={handleChangeForm}
            value={props.item ? editProductDetails.name : productDetails.name}
            required
            onBlur={(event) => handleBlur(event)}
          ></FormInput>
        </div>
        <div className="modal__cell">
          <label htmlFor="product-size">Product Size</label>
          <FormInput
            type="text"
            id="modal-product-size"
            name="size"
            placeholder="eg Medium"
            onChange={handleChangeForm}
            onBlur={(event) => handleBlur(event)}
            value={props.item ? editProductDetails.size : productDetails.size}
            required
          ></FormInput>
        </div>
        <div className="modal__cell">
          <label htmlFor="product-price">Product Price</label>
          <FormInput
            type="text"
            id="modal-product-price"
            name="price"
            placeholder="eg 2.00"
            onChange={handleChangeForm}
            onBlur={(event) => handleBlur(event)}
            value={props.item ? editProductDetails.price : productDetails.price}
            required
          ></FormInput>
        </div>
        <div className="modal__cell">
          <label htmlFor="product-category">Product Category</label>
          <FormInput
            type="text"
            id="modal-product-category"
            name="category"
            placeholder="eg Bath Bombs"
            onChange={handleChangeForm}
            onBlur={(event) => handleBlur(event)}
            value={
              props.item ? editProductDetails.category : productDetails.category
            }
            required
          ></FormInput>
        </div>
        <div className="modal__cell">
          <label htmlFor="product-link">Product Link</label>
          <FormInput
            type="text"
            id="modal-product-link"
            name="link"
            placeholder="eg strawberry-medium"
            onChange={handleChangeForm}
            onBlur={(event) => handleBlur(event)}
            value={props.item ? editProductDetails.link : productDetails.link}
            required
          ></FormInput>
        </div>
        <div className="modal__cell">
          <label htmlFor="product-tag">Product Tag</label>
          <FormInput
            type="text"
            id="modal-product-tag"
            name="tag"
            placeholder="eg Fruity"
            onChange={handleChangeForm}
            onBlur={(event) => handleBlur(event)}
            value={props.item ? editProductDetails.tag : productDetails.tag}
            required
          ></FormInput>
        </div>

        {props.crudMethod !== "delete" ? (
          <div className="modal__cell">
            <label htmlFor="product-image">Product Image</label>
            <div className="modal__cell--image-wrapper">
              <input
                type="file"
                id="productImageUpload"
                name="image"
                placeholder="Add File"
                hidden="hidden"
                value={productDetails.image}
                onChange={handleChangeForm}
              ></input>
              <Button
                id="image-upload-button"
                textValue="Upload Image"
                onClickEvent={handleImageUpload}
                helperText={null}
              >
                Upload Image
              </Button>
            </div>
          </div>
        ) : null}
        <div className="modal__button-wrapper">
          {props.crudMethod !== "delete" ? (
            <Button
              id="model-add-update-button"
              textValue={props.item ? "Update Product" : "Add Product"}
              helperText={helperText}
              onClickEvent={handleSubmit}
            />
          ) : (
            <Aux>
              <p>Are you sure you want to delete?</p>
              <Button
                id="model-add-update-button"
                textValue={"Delete"}
                helperText={null}
                onClickEvent={handleSubmit}
              />
            </Aux>
          )}
        </div>
      </form>
    );
  }

  if (props.type === "category") {
    return (
      <form
        id="category-form"
        className="modal--category"
        onSubmit={handleSubmit}
      >
        <div className="modal__cell">
          <label htmlFor="product-category">Category Name</label>
          <FormInput
            type="text"
            id="modal-category"
            name="category"
            placeholder="eg Bath Bombs"
            onChange={handleChangeForm}
            onBlur={(event) => handleBlur(event)}
            value={
              props.item
                ? editCategoryDetails.category
                : categoryDetails.category
            }
            required
          ></FormInput>
        </div>
        <div className="modal__cell">
          <label htmlFor="product-link">Category Link</label>
          <FormInput
            type="text"
            id="modal-link"
            name="link"
            placeholder="eg bath-bombs"
            onChange={handleChangeForm}
            onBlur={(event) => handleBlur(event)}
            value={props.item ? editCategoryDetails.link : categoryDetails.link}
            required
          ></FormInput>
        </div>

        <div className="modal__button-wrapper">
          {props.crudMethod !== "delete" ? (
            <Button
              id="modal-category-button"
              textValue={props.item ? "Update Category" : "Add Category"}
              onClickEvent={handleSubmit}
            />
          ) : (
            <Aux>
              <p>Are you sure you want to delete?</p>
              <Button
                id="modal-category-button"
                textValue={"Delete"}
                onClickEvent={handleSubmit}
              />
            </Aux>
          )}
        </div>
      </form>
    );
  }
};

export default CrudModal;
