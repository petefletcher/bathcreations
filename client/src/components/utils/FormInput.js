import React from "react";
import "../../styles/FormInput.scss";

const FormInput = ({ handleChange, label, ...otherProps }) => {
  return (
    <div className="group">
      <input
        className="group__form-input"
        onChange={handleChange}
        {...otherProps}
      />
      <span id={`helper-text`} className="helper-text"></span>
    </div>
  );
};

export default FormInput;
