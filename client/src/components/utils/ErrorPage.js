import React from "react";

function ErrorPage() {
  return <div data-testid="404-not-found">404 Not Found</div>;
}

export default ErrorPage;
