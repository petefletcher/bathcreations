import React from "react";

function Button(props) {
  return (
    <button
      id={props.id}
      className="button"
      type="button"
      value={props.textValue}
      aria-label={props.textValue}
      onClick={(event) => props.onClickEvent(event)}
    >
      {props.textValue}
    </button>
  );
}

export default Button;
