import React, { useState } from "react";
import "../../styles/ProductMapper.scss";
import classnames from "classnames";

// redux
import { useDispatch, useSelector } from "react-redux";
import { addItem } from "../../redux/cart/cart.action";

// components
import Dot from "./Dot";
import Aux from "../../hoc/AuxComponent";

const ProductMapper = ({ id, item }) => {
  // redux hooks
  const currentCart = useSelector((state) => state.cart.cartItems);

  const dispatch = useDispatch();

  // react hooks
  const [productPrice, setProductPrice] = useState(2);

  const handleSetPrice = (event) => {
    if (!event.target.className.includes("Medium")) {
      setIsMedium(false);
      setProductPrice(2.5);
    } else {
      setIsMedium(true);
      setProductPrice(2);
    }
  };

  const handleAddToBasket = (item) => {
    dispatch({ type: addItem, payload: item });
  };

  const handleRenderDots = () => {
    switch (item.category) {
      case "Bath Bomb":
        if (item.size === "Medium") {
          return <Dot name="product-item__price--dot-medium" textValue="M" />;
        } else {
          return <Dot name="product-item__price--dot-large" textValue="L" />;
        }
      default:
        break;
    }
  };

  return (
    <Aux>
      <div className="product-item">
        <div className="product-item__img-container">
          <img className="product-item__img" src={item.imageUrl} />
        </div>

        <div className="product-item__content">
          <div className="product-item__name">
            {item.name}
            <button
              className="button"
              onClick={() => handleAddToBasket(item)}
              id={id}
            >
              Add To Basket
            </button>
          </div>

          <div className={`product-item__price`}>
            {/* <div className="product-item__price--dot-selector-wrapper">
              {handleRenderDots()}
            </div> */}
            <div className="product-item__price--text">{item.size}</div>
            <div className="product-item__price--text">£{item.price}</div>
          </div>
        </div>
      </div>
    </Aux>
  );
};

export default ProductMapper;
