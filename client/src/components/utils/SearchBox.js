import React, { useState } from "react";
import "../../styles/SearchBox.scss";
import Button from "./Button";
import FormInput from "./FormInput";

const SearchBox = (props) => {
  const [placeHolderTextValue, setPlaceHolderTextValue] = useState("");

  const handleChangeForm = (event) => {
    const { value } = event.target;
    //set state with changed value
    setPlaceHolderTextValue(value);
    //const { id, value } = event.target;
  };

  const submitForm = (event) => {
    event.preventDefault();
    //Reset search value

    setPlaceHolderTextValue("");
  };

  return props.open ? (
    <div
      className={props.modifier ? `searchbox${props.modifier}` : "searchbox"}
    >
      <form
        className="searchbox__form"
        aria-label="Search"
        onSubmit={(event) => submitForm(event)}
      >
        <FormInput
          parent="searchbox"
          id="search"
          type="text"
          placeholder="Search Bath Creations"
          aria-labelledby="search-button"
          onChange={handleChangeForm}
          value={placeHolderTextValue}
        />
        <Button
          id="search-button"
          textValue="Search"
          onClickEvent={submitForm}
        />
      </form>
    </div>
  ) : null;
};

export default SearchBox;
