export const formValidation = (event, type) => {
  const { value, id, name } = event.target;
  const validated = false;

  // check link is proper format
  if (name === "link") {
    if (!checkLinkFormat(value, id, type)) return;
    if (checkLowerCase(value, id)) return;

    return true;
  } else if (name === "price") {
    if (checkPrice(value, id)) return;
    return true;
  } else {
    // check if is empty
    if (checkIfEmpty(value, id)) return;
    // checkif it has only letters
    if (!checkIfOnlyLetters(value, id)) return;
    // check if capital is required
    if (checkCapitals(value, id)) return;
    // check image is uploaded?

    return true;
  }
};

const checkIfEmpty = (field, id) => {
  if (isEmpty(field.trim())) {
    //set field invalid
    setInvalid(field, `Field must not be empty`, id);
    return true;
  } else {
    // set field valid
    setValid(field, null, id);
    return false;
  }
};

const isEmpty = (value) => {
  if (value === "") return true;
  return false;
};

const setInvalid = (field, message, id) => {
  const element = document.getElementById(id);
  const sibling = document.getElementById(id).nextElementSibling;
  element.classList.add("invalid");

  sibling.style.fontSize = "12px";
  sibling.style.marginLeft = "5px";
  sibling.style.marginTop = "0px";
  sibling.style.marginBottom = "0px";
  sibling.style.color = "red";
  sibling.innerHTML = message;
};

const setValid = (field, message, id) => {
  const element = document.getElementById(id);
  element.classList.add("valid");

  const sibling = document.getElementById(id).nextElementSibling;
  sibling.innerHTML = "";
};

const checkIfOnlyLetters = (field, id) => {
  if (/^[A-Za-z ]+$/.test(field)) {
    setValid(field, null, id);
    return true;
  } else {
    setInvalid(field, `field must be only contain letters`, id);
    return false;
  }
};

const checkLinkFormat = (field, id, type) => {
  const checkHyphen = field.split("-");

  if (checkHyphen.length > 1) {
    setValid(field, null, id);
    return true;
  } else {
    if (type === "product") {
      setInvalid(field, `Seperate words with '-' for product link`, id);
      return false;
    }
  }
};

const checkLowerCase = (field, id) => {
  const fieldLower = field.toLowerCase();

  if (field === fieldLower) {
    setValid(field, null, id);
    return true;
  } else {
    setInvalid(field, "Link needs to be all lower case", id);
    return false;
  }
};

const checkCapitals = (field, id) => {
  const fieldArray = field.split(" ");

  fieldArray.every((fieldWord) => {
    const fieldLower = fieldWord.toLowerCase();
    if (fieldWord === fieldLower) {
      setInvalid(field, `Each word must start with capital`, id);
      return false;
    } else {
      setValid(field, null, id);
      return true;
    }
  });
};

const checkPrice = (field, id) => {
  const checkPrice = field.split(".");
  if (checkPrice.length > 1) {
    if (checkPrice[1].length === 2) {
      setValid(field, null, id);
      return true;
    } else {
      setInvalid(field, `The price needs 2 decimal places eg 2.00`, id);
      return false;
    }
  } else {
    setInvalid(field, `There needs to be a decimal place eg 2.00`, id);
    return false;
  }
};
