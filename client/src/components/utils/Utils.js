import React from "react";
import ProductMapper from "./ProductMapper";
import CategoryItem from "../CategoryItem";

export const renderCategoryProducts = (category, products) => {
  const findItemsWithCategory = products.filter(
    (item) => item.category === category
  );
  return findItemsWithCategory.map((itemWithCat) => {
    return (
      <div className="bath-bombs__wrapper" key={itemWithCat.id}>
        <ProductMapper id={itemWithCat.id} item={itemWithCat} />
      </div>
    );
  });
};

export const renderCategories = (products) => {
  return products.map((categories, i) => {
    const { id, link, category } = categories;

    return (
      <CategoryItem
        key={id}
        categoryKey={id}
        category={category}
        link={link}
        image={i <= 4 ? "--imgbg1" : "--imgbg2"}
      />
    );
  });
};
