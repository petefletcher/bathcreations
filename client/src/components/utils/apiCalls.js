import axios from "axios";
axios.defaults.baseURL =
  "https://europe-west2-bath-creations.cloudfunctions.net/app";

axios.defaults.headers.post["Access-Control-Allow-Origin"] = "*";

let token = "";
const storedToken = localStorage.getItem("bcToken");

if (storedToken) {
  token = storedToken;
}

const BASE_URL_FIREBASE =
  process.env.NODE_ENV === "development"
    ? "http://localhost:5001/bath-creations/europe-west2/app/v1"
    : "/v1";

/** Payments */
export const paypalPaymentsCall = async (cartItems, cartTotal) => {
  const { data } = await axios.post(`${BASE_URL_FIREBASE}/payments`, {
    cartItems: [...cartItems],
    cartAmount: {
      currency: "GBP",
      total: cartTotal,
    },
  });
  return data;
};

export const getCompleteTransactionPaypal = async (transactionId) => {
  const { data } = await axios.get(
    `${BASE_URL_FIREBASE}/transactions?${transactionId}`
  );
  return data;
};

export const getCompleteTransactionStripe = async (transactionId) => {
  const { data } = await axios.get(
    `${BASE_URL_FIREBASE}/get-transaction?id=${transactionId}`
  );
  return data;
};

export const getAllTransactions = async () => {
  const { data } = await axios.get(
    `${BASE_URL_FIREBASE}/all-transactions?type=transactions`,
    {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    }
  );

  return data;
};

export const stripePayment = async (token, priceForStripe, items) => {
  const { data } = await axios.post(`${BASE_URL_FIREBASE}/stripePayment?`, {
    amount: priceForStripe,
    token,
    items,
  });
  return data;
};

/** Categories */
export const getCategories = async () => {
  const { data } = await axios.get(
    `${BASE_URL_FIREBASE}/categories?type=categories`
  );
  return data;
};

/** Products */
export const getProducts = async () => {
  const { data } = await axios.get(
    `${BASE_URL_FIREBASE}/products?type=products`
  );

  return data;
};

// export const addProductToCollection = async (formData, headers) => {
//   const { data } = await axios.post(
//     `${BASE_URL_FIREBASE}/products?type=products`,
//     {
//       formData,
//     },
//     {
//       headers: {
//         Authorization: `Bearer ${token}`,
//         "Content-Type":
//           "multipart/form-data; boundary=----WebKitFormBoundaryyXT3ROcchINFzwuu",
//       },
//     }
//   );
//   return data;
// };

export const addCategoryToCollection = async (categoryData) => {
  const { data } = await axios.post(
    `${BASE_URL_FIREBASE}/categories`,
    {
      categoryData,
    },
    {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    }
  );
  return data;
};

export const editCategoryInCollection = async (categoryData) => {
  const { data } = await axios.put(
    `${BASE_URL_FIREBASE}/categories/edit-category`,
    {
      categoryData,
    },
    {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    }
  );
  return data;
};

export const editProductInCollection = async (productData) => {
  const { data } = await axios.put(
    `${BASE_URL_FIREBASE}/products/edit-product`,
    {
      productData,
    },
    {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    }
  );
  return data;
};

export const deleteItemFromCollection = async (itemData, type) => {
  let itemType;

  if (type === "product") {
    itemType = "products";
  } else {
    itemType = "categories";
  }

  const { data } = await axios.delete(
    `${BASE_URL_FIREBASE}/${itemType}/remove-${type}?item=${itemData.link}&type=${itemType}`,
    {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    }
  );
  return data;
};

/** User */
export const signInAdminEmailAndPassword = async (
  signinEmail,
  signinPassword
) => {
  const { data } = await axios.post(`${BASE_URL_FIREBASE}/admin-login`, {
    email: signinEmail,
    password: signinPassword,
  });
  return data;
};

export const getSession = async (token) => {
  const { data } = await axios.post(`${BASE_URL_FIREBASE}/user-session`, {
    token,
  });
  return data;
};

export const verifyAdmin = async (email, token) => {
  const { data } = await axios.post(
    `${BASE_URL_FIREBASE}/verify-admin`,
    {
      email,
      token,
    },
    {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    }
  );
  return data;
};

export const addProductToCollection = (formData) => {
  var myHeaders = new Headers();
  myHeaders.append("Authorization", `Bearer ${token}`);

  var requestOptions = {
    method: "POST",
    headers: myHeaders,
    body: formData,
    redirect: "follow",
    mode: "cors",
  };

  const response = fetch(
    "http://localhost:5001/bath-creations/europe-west2/app/v1/products?type=products",
    requestOptions
  )
    .then(async (res) => {
      const data = await res.json();
      if (res.ok) {
        console.log("DATA OK", data);
        return data;
      } else {
        throw new Error(data.message);
      }
    })
    .catch((error) => {
      return Promise.reject(error);
    });
  return response;
};
