import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import Button from "./utils/Button";
import "../styles/Information.scss";

function Information() {
  const [information, setCategories] = useState([
    { section: "About", link: "about" },
    { section: "Contact Us", link: "contact-us" },
    { section: "FAQ", link: "faq" },
  ]);

  const history = useHistory();

  const handleClick = (event) => {
    const { id } = event.target;
    history.push(`/${id}`);
  };

  return (
    <div className="additional-information">
      <h3 id="additional-information" role="additional information section">
        Stay Informed
      </h3>
      <div
        className="additional-information__info-wrapper"
        data-testid="home-page-stay-informed"
      >
        {information.map((element, i) => {
          return (
            <div
              id={element.link}
              className="additional-information__info-row"
              key={`section-${i}`}
              data-testid={`section-${i}`}
              onClick={handleClick}
            >
              <Button
                className="additional-information__button"
                textValue={element.section}
                id={element.link}
                onClickEvent={handleClick}
              />
            </div>
          );
        })}
      </div>
    </div>
  );
}

export default Information;
