import React from "react";
import "../../styles/AdminSignIn.scss";

import SignIn from "../SignIn";

const AdminSignIn = () => {
  return (
    <div className="admin-sign-in">
      <div className="admin-sign-in__wrapper">
        <SignIn />
      </div>
    </div>
  );
};

export default AdminSignIn;
