import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  fetchTransactionsStart,
  getTransactionData,
} from "../../redux/cart/cart.action";

import Spinner from "../../hoc/LoadingSpinner";
import Button from "../utils/Button";
import CrudModal from "../utils/Modal";
import Aux from "../../hoc/AuxComponent";
import "../../styles/ManageItems.scss";

const Transactions = (props) => {
  const dispatch = useDispatch();

  const hasTransactionsLoaded = useSelector((state) => !!state.cart.hasLoaded);

  const getTransactions = useSelector((state) => state.cart.transactions);

  const [showItems, setShowItems] = useState(false);
  const [viewItem, setViewItem] = useState([]);
  const [itemId, setItemId] = useState(null);

  useEffect(() => {
    dispatch(fetchTransactionsStart());
  }, []);

  const handleShowItems = (event) => {
    const { id } = event.target;
    setShowItems(!showItems);
    findItemList(id);
  };

  const findItemList = (id) => {
    const item = getTransactions.find((element) => {
      if (element.id === id) {
        return element;
      }
    });

    setViewItem(item);
  };

  return (
    <>
      <div className="manage">
        <div className="manage__heading">
          <h3>Welcome To Your Transactions</h3>
          <div className="manage__wrapper">
            {hasTransactionsLoaded && getTransactions.length > 0 ? (
              getTransactions.map((transaction, i) => {
                return (
                  <Aux key={i}>
                    <div className="manage__transaction">
                      <div className="manage__transaction-cell">
                        <div className="manage__transaction-inner-cell-name">
                          Transaction ID
                        </div>
                        <div className="manage__transaction-inner-cell">
                          {transaction.id}
                        </div>
                      </div>
                      <div className="manage__transaction-cell">
                        <div className="manage__transaction-inner-cell-name">
                          Payee ID
                        </div>
                        <div className="manage__transaction-inner-cell">
                          {transaction.payee.name
                            ? transaction.payee.name
                            : transaction.payee.email}
                        </div>
                      </div>
                      <div className="manage__transaction-cell">
                        <div className="manage__transaction-inner-cell-name">
                          Date Completed
                        </div>
                        <div className="manage__transaction-inner-cell">
                          {Date(transaction.createdAt).slice(0, -31)}
                        </div>
                      </div>
                      <div className="manage__transaction-cell">
                        <div className="manage__transaction-inner-cell-name">
                          Total
                        </div>
                        <div className="manage__transaction-inner-cell">
                          {transaction.amount.total}
                        </div>
                      </div>
                      <div className="manage__transaction-cell">
                        <Button
                          onClickEvent={handleShowItems}
                          id={`${transaction.id}`}
                          textValue={`View Items`}
                        />

                        {showItems && transaction.id === viewItem.id ? (
                          <div className="manage__transaction-cell">
                            <div className="manage__items-wrapper">
                              {viewItem.item_list.items.map((element, i) => {
                                return (
                                  <div
                                    className="manage__transaction-items"
                                    key={`${element}-${i}`}
                                  >
                                    <div className="manage__name">
                                      {element.name}
                                    </div>
                                    <div className="manage__price">
                                      {element.price}
                                    </div>
                                  </div>
                                );
                              })}
                            </div>
                          </div>
                        ) : null}
                      </div>
                    </div>
                  </Aux>
                );
              })
            ) : (
              <div className="spinner-wrapper">
                <Spinner />
              </div>
            )}
          </div>
        </div>
      </div>
    </>
  );
};

Transactions.propTypes = {};

export default Transactions;
