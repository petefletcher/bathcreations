import React, { useEffect } from "react";
import { renderCategoryProducts } from "../utils/Utils";
import { fetchProductsStart } from "../../redux/products/products.action";
import { useDispatch, useSelector } from "react-redux";

import Spinner from "../../hoc/LoadingSpinner";

import Aux from "../../hoc/AuxComponent";

// this is not specific to bathbombs
import "../../styles/BathBombs.scss";

function Sponges() {
  const hasProductsLoaded = useSelector(
    (state) => !!state.allProducts.products
  );
  const getProducts = useSelector((state) => state.allProducts.products);

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchProductsStart());
  }, []);

  return (
    <Aux>
      <h1>Sponges</h1>
      {hasProductsLoaded ? (
        <div className="bath-bombs">
          {renderCategoryProducts("Sponges", getProducts)}
        </div>
      ) : (
        <div className="spinner-wrapper">
          <Spinner />
        </div>
      )}
    </Aux>
  );
}

export default Sponges;
