import React from "react";
import "../../styles/SignInSignUp.scss";

import SignIn from "../SignIn";
import SignUp from "../SignUp";

const SignInSignUp = () => {
  return (
    <div className="signin-signup">
      <div className="signin-signup__signin-wrapper">
        <SignIn />
      </div>
      <div className="signin-signup__signup-wrapper">
        <SignUp />
      </div>
    </div>
  );
};

export default SignInSignUp;
