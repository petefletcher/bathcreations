import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  showTheModal,
  hideTheModal,
} from "../../redux/products/products.action";

import Spinner from "../../hoc/LoadingSpinner";
import Button from "../utils/Button";
import CrudModal from "../utils/Modal";
import Aux from "../../hoc/AuxComponent";
import "../../styles/ManageItems.scss";

const ManageProducts = (props) => {
  const dispatch = useDispatch();

  const hasProductsLoaded = useSelector(
    (state) => !!state.allProducts.products
  );

  const getProducts = useSelector((state) => state.allProducts.products);
  const showModal = useSelector((state) => state.allProducts.modalVisible);

  const [crudModal, setCrudMethod] = useState(null);
  const [item, setItem] = useState(null);

  const handleModal = (method, singleItem) => {
    if (!showModal) {
      setCrudMethod(method);
      setItem(singleItem);
      document.querySelector("body").style.overflow = "hidden";
      dispatch(showTheModal());
    } else {
      setCrudMethod(null);
      setItem(null);
      document.querySelector("body").style.overflow = "visible";
      dispatch(hideTheModal());
    }
  };

  return (
    <>
      {showModal ? (
        <Aux>
          <div className="backdrop" onClick={handleModal}></div>
          <CrudModal
            type={`product`}
            crudMethod={crudModal}
            item={item}
            clicked={handleModal}
          />
        </Aux>
      ) : null}
      <div className="manage">
        <div className="manage__heading">
          <h3>Welcome To Your Products</h3>
          <div className="manage__add-button">
            <button
              className="button"
              type="button"
              aria-label={`add-product-button`}
              onClick={() => handleModal("add")}
            >
              Add Product
            </button>
          </div>
          <div className="manage__wrapper">
            {hasProductsLoaded ? (
              getProducts.map((product, i) => {
                return (
                  <Aux key={i}>
                    <div className="manage__product">
                      <div className="manage__cell">
                        <div className="manage__inner-cell-name">
                          Product Name
                        </div>
                        <div className="manage__inner-cell">{product.name}</div>
                      </div>
                      <div className="manage__cell">
                        <div className="manage__inner-cell-name">
                          Product ID
                        </div>
                        <div className="manage__inner-cell">{product.id}</div>
                      </div>
                      <div className="manage__cell">
                        <div className="manage__inner-cell-name">
                          Product Price
                        </div>
                        <div className="manage__inner-cell">
                          {product.price}
                        </div>
                      </div>
                      <div className="manage__cell">
                        <div className="manage__inner-cell-name">
                          Product Category
                        </div>
                        <div className="manage__inner-cell">
                          {product.category}
                        </div>
                      </div>
                      <div className="manage__cell">
                        <div className="manage__inner-cell-name">
                          Product Link
                        </div>
                        <div className="manage__inner-cell">{product.link}</div>
                      </div>
                      <div className="manage__cell">
                        <div className="manage__inner-cell-name">
                          Product Image
                        </div>
                        <div className="manage__inner-cell">
                          <img src={product.imageUrl}></img>
                        </div>
                      </div>
                      <div className="manage__button-wrapper">
                        <button
                          id={product.id}
                          className="button"
                          type="button"
                          aria-label={`edit-product-button`}
                          onClick={() => handleModal("edit", product)}
                        >
                          Edit
                        </button>
                        <button
                          id={product.id}
                          className="button"
                          type="button"
                          aria-label={`delete-product-button`}
                          onClick={() => handleModal("delete", product)}
                        >
                          Delete
                        </button>
                      </div>
                    </div>
                  </Aux>
                );
              })
            ) : (
              <div className="spinner-wrapper">
                <Spinner />
              </div>
            )}
          </div>
        </div>
      </div>
    </>
  );
};

ManageProducts.propTypes = {};

export default ManageProducts;
