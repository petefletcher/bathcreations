import React, { useEffect } from "react";
import Aux from "../../hoc/AuxComponent";
import Spinner from "../../hoc/LoadingSpinner";
import { renderCategoryProducts } from "../utils/Utils";
import { fetchProductsStart } from "../../redux/products/products.action";
import { useDispatch, useSelector } from "react-redux";
import "../../styles/BathBombs.scss";

const BathBombs = () => {
  const hasProductsLoaded = useSelector(
    (state) => !!state.allProducts.products
  );

  const getProducts = useSelector((state) => state.allProducts.products);

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchProductsStart());
  }, []);

  return (
    <Aux>
      <h1>Bath Bombs</h1>
      {hasProductsLoaded ? (
        <div className="bath-bombs">
          {renderCategoryProducts("Bath Bombs", getProducts)}
        </div>
      ) : (
        <div className="spinner-wrapper">
          <Spinner />
        </div>
      )}
    </Aux>
  );
};

export default BathBombs;
