import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  showTheModal,
  hideTheModal,
} from "../../redux/products/products.action";

import Spinner from "../../hoc/LoadingSpinner";
import Button from "../utils/Button";
import CrudModal from "../utils/Modal";
import Aux from "../../hoc/AuxComponent";
import "../../styles/ManageItems.scss";

const ManageCategories = (props) => {
  const dispatch = useDispatch();

  const hasCategoriesLoaded = useSelector(
    (state) => !!state.allCategory.categories
  );

  const getCategories = useSelector((state) => state.allCategory.categories);
  //this could be a utilty reducer
  const showModal = useSelector((state) => state.allProducts.modalVisible);
  const [crudModal, setCrudMethod] = useState(null);
  const [item, setItem] = useState(null);

  const handleModal = (method, singleItem) => {
    if (!showModal) {
      setCrudMethod(method);
      setItem(singleItem);
      dispatch(showTheModal());
    } else {
      setCrudMethod(null);
      setItem(null);
      dispatch(hideTheModal());
    }
  };

  return (
    <>
      {showModal ? (
        <Aux>
          <div className="backdrop" onClick={handleModal}></div>
          <CrudModal
            type={`category`}
            crudMethod={crudModal}
            item={item}
            clicked={handleModal}
          />
        </Aux>
      ) : null}
      <div className="manage">
        <div className="manage__heading">
          <h3>Welcome To Your Categories</h3>
          <div className="manage__add-button">
            <button
              className="button"
              type="button"
              aria-label={`add-product-button`}
              onClick={() => handleModal("add")}
            >
              Add Category
            </button>
          </div>
          <div className="manage__wrapper">
            {hasCategoriesLoaded ? (
              getCategories.map((category, i) => {
                return (
                  <Aux key={i}>
                    <div className="manage__category">
                      <div className="manage__cell">
                        <div className="manage__inner-cell-name">
                          Category Name
                        </div>
                        <div className="manage__inner-cell">
                          {category.category}
                        </div>
                      </div>
                      <div className="manage__cell">
                        <div className="manage__inner-cell-name">
                          Category ID
                        </div>
                        <div className="manage__inner-cell">{category.id}</div>
                      </div>

                      <div className="manage__cell">
                        <div className="manage__inner-cell-name">
                          Category Link
                        </div>
                        <div className="manage__inner-cell">
                          {category.link}
                        </div>
                      </div>

                      <div className="manage__button-wrapper">
                        <button
                          id={category.id}
                          className="button"
                          type="button"
                          aria-label={`edit-product-button`}
                          onClick={() => handleModal("edit", category)}
                        >
                          Edit
                        </button>
                        <button
                          id={category.id}
                          className="button"
                          type="button"
                          aria-label={`delete-product-button`}
                          onClick={() => handleModal("delete", category)}
                        >
                          Delete
                        </button>
                      </div>
                    </div>
                  </Aux>
                );
              })
            ) : (
              <div className="spinner-wrapper">
                <Spinner />
              </div>
            )}
          </div>
        </div>
      </div>
    </>
  );
};

ManageCategories.propTypes = {};

export default ManageCategories;
