import React from "react";
import "@testing-library/jest-dom/extend-expect";
import { BrowserRouter as Router } from "react-router-dom";
import { fireEvent } from "@testing-library/react";
import { createMemoryHistory } from "history";
import { render } from "../__tests__/test-utils";

import SiginIn from "../SignIn";
describe("<Signin />", () => {
  it("should allow use to sign in if the user exists", () => {
    const history = createMemoryHistory();

    window.location = jest.fn().mockImplementation(() => true);

    const onClickEvent = jest.fn();

    const { getByLabelText, getByText } = render(
      <Router history={history}>
        <SiginIn submitForm={onClickEvent()} />
      </Router>,
      {
        initialState: {
          error: {
            error: null,
          },
          user: {
            currentUser: null,
            claim: false,
          },
        },
      }
    );

    const emailInput = getByLabelText("email-sign-in-input");
    const passwordInput = getByLabelText("password-sign-in-input");

    expect(emailInput.value).toBe(""); // empty before
    fireEvent.change(emailInput, { target: { value: "test@test.com" } });
    expect(emailInput.value).toBe("test@test.com"); //empty after

    expect(passwordInput.value).toBe(""); // empty before
    fireEvent.change(passwordInput, { target: { value: "password" } });
    expect(passwordInput.value).toBe("password"); //empty after

    fireEvent.click(getByText(/Sign In/));
    expect(onClickEvent).toHaveBeenCalled();
  });

  it("should alert the user if they cannot sign in", () => {
    const history = createMemoryHistory();

    window.alert = jest.fn();

    const { debug } = render(
      <Router history={history}>
        <SiginIn />
      </Router>,
      {
        initialState: {
          error: {
            error: { message: "Error test" },
          },
          user: {
            currentUser: null,
            claim: false,
          },
        },
      }
    );
    jest.spyOn(window, "alert");
    expect(window.alert).toHaveBeenCalled();
  });
});
