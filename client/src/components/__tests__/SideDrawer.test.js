import React from "react";
import "@testing-library/jest-dom/extend-expect";
import { render, fireEvent } from "@testing-library/react";
import SideDrawer from "../navigation/SideDrawer/SideDrawer";

describe("SideDrawer", () => {
  it("should not be visible on load", () => {
    const { container } = render(<SideDrawer />);
    expect(container.firstChild.innerHTML).toContain(
      "side-drawer side-drawer__close"
    );
  });

  it("should be visible when theprops open are true has been clicked", () => {
    const { container } = render(<SideDrawer open={true} />);

    expect(container.firstChild.innerHTML).toContain(
      "side-drawer side-drawer__open"
    );
  });

  it("should navigate to user page if signed in", () => {});
  it("should navigate to sign in page if not signed in", () => {});
  it("should navigate to basket page", () => {});
});
