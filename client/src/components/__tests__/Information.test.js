import React from "react";
import "@testing-library/jest-dom/extend-expect";
import { render, fireEvent } from "@testing-library/react";
import { Router } from "react-router-dom";
import Information from "../Information";

describe("Home page Information Testing", () => {
  const historyMock = { push: jest.fn(), location: {}, listen: jest.fn() };

  it("should buy now clickable button for each of the cards", () => {
    const handleClick = jest.fn();

    const { getByTestId } = render(
      <Router history={historyMock}>
        <Information onClickEvent={handleClick()} />
      </Router>
    );

    const InformationSection = getByTestId("home-page-stay-informed");

    expect(InformationSection.children[0].tagName).toBe("DIV");
    expect(InformationSection.children[1].tagName).toBe("DIV");
    expect(InformationSection.children[2].tagName).toBe("DIV");

    fireEvent.click(InformationSection.children[0].firstChild);
    expect(handleClick).toHaveBeenCalledTimes(1);
    fireEvent.click(InformationSection.children[1].firstChild);
    expect(handleClick).toHaveBeenCalledTimes(1);
    fireEvent.click(InformationSection.children[2].firstChild);
    expect(handleClick).toHaveBeenCalledTimes(1);
  });

  it("Information snapshot", () => {
    const { container } = render(
      <Router history={historyMock}>
        <Information />
      </Router>
    );
    expect(container.firstChild).toMatchSnapshot();
  });
});
