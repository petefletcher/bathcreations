import React from "react";
import "@testing-library/jest-dom/extend-expect";
import { render } from "@testing-library/react";
import NavigationItems from "../navigation/navigationItems/NavigationItems";

const resizeWindow = (width) => {
  window.innerWidth = width;
  window.dispatchEvent(new Event("resize"));
};

describe("NavigationItems", () => {
  it("should have allow props to modify classname for side-bar", () => {
    const { container } = render(<NavigationItems class="__side-bar" />);

    expect(container.firstChild).toHaveClass("navigation-items__side-bar");
  });

  it("should have allow props to modify classname for navbar", () => {
    const { container } = render(<NavigationItems class="__navbar" />);

    expect(container.firstChild).toHaveClass("navigation-items__navbar");
  });

  it("should be able to modify icon size based on class and screensize", () => {
    global.innerWidth = 400;

    const { container, rerender } = render(
      <NavigationItems class="__navbar">
        <i></i>
      </NavigationItems>
    );

    expect(container.innerHTML).toContain("la-3x");

    resizeWindow(500);
    rerender(<NavigationItems />);
    expect(container.innerHTML).toContain("la-4x");
  });
});
