import React from "react";
import "@testing-library/jest-dom/extend-expect";
import { render, fireEvent } from "@testing-library/react";
import Logo from "../../logo/Logo";

describe("Logo", () => {
  it("should contain logo img file", () => {
    const { getByRole } = render(<Logo />);

    expect(getByRole("img")).toBeInTheDocument();
  });

  it("should have props passed to it", () => {
    const { container } = render(<Logo jc="center" />);
    expect(container.firstChild).toHaveAttribute("style");
  });
});
