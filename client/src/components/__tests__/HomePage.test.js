import React from "react";
import "@testing-library/jest-dom/extend-expect";
import { render } from "./test-utils";
import HomePage from "../../containers/HomePage";
import Categories from "../Categories.js";

describe("<Hompeage/> Render Categories as part of the homepage", () => {
  it("should render categories if categories loaded in reducer", () => {
    const { getByTestId } = render(<HomePage><Categories /></HomePage>, {
      initialState: {
        allCategory: {
          categories: [
            {
              category: "Bath Bombs",
              createdAt: "2020-09-07T15:51:51.541Z",
              id: "bath-bombs",
              link: "bath-bombs",
            },
          ],
        },
      },
    });
    
    expect(getByTestId("category-item")).toBeInTheDocument();
  });

})

