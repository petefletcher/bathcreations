import React from "react";
import { BrowserRouter as Router } from "react-router-dom";
import "@testing-library/jest-dom/extend-expect";
import { render } from "../__tests__/test-utils";
import App from "../App";
import { createMemoryHistory } from "history";
import jwtDecode from "jwt-decode";

let mockDate = new Date();

jest.mock("jwt-decode");

const mockJwt = require("jwt-decode");

describe("App - React router testing", () => {
  it("full app rendering with admin token", () => {
    localStorage.setItem("bcToken", "qwerty");

    mockJwt.mockImplementation(() => {
      return { exp: mockDate.getTime(), admin: true };
    });

    const history = createMemoryHistory();
    const { container } = render(
      <Router history={history}>
        <App />
      </Router>,
      {
        initialState: {
          allCategory: {
            categories: [
              {
                category: "Bath Bombs",
                createdAt: "2020-09-07T15:51:51.541Z",
                id: "bath-bombs",
                link: "bath-bombs",
              },
            ],
          },
        },
      }
    );

    // verify page content for expected route
    // often you'd use a data-testid or role query, but this is also possible
    expect(container.innerHTML).toMatch("Categories");
    //verify we have checked the token
    expect(jwtDecode).toHaveBeenCalled();
  });

  it("full app rendering without token", () => {
    localStorage.setItem("bcToken", "qwerty");

    mockJwt.mockImplementation(() => {
      return { exp: mockDate.getTime(), admin: false };
    });

    const history = createMemoryHistory();
    const { container } = render(
      <Router history={history}>
        <App />
      </Router>,
      {
        initialState: {
          allCategory: {
            categories: [
              {
                category: "Bath Bombs",
                createdAt: "2020-09-07T15:51:51.541Z",
                id: "bath-bombs",
                link: "bath-bombs",
              },
            ],
          },
        },
      }
    );

    // verify page content for expected route
    // often you'd use a data-testid or role query, but this is also possible
    expect(container.innerHTML).toMatch("Categories");
    //verify we have checked the token
    expect(jwtDecode).toHaveBeenCalled();
  });

  it("should render but signout user it the token has expired", () => {
    let pastDate = mockDate.getDate() - 7;
    mockDate.setDate(pastDate);

    mockJwt.mockImplementation(() => {
      return { exp: 1, admin: false };
    });

    const history = createMemoryHistory();
    const { container } = render(
      <Router history={history}>
        <App />
      </Router>,
      {
        initialState: {
          allCategory: {
            categories: [
              {
                category: "Bath Bombs",
                createdAt: "2020-09-07T15:51:51.541Z",
                id: "bath-bombs",
                link: "bath-bombs",
              },
            ],
          },
        },
      }
    );

    // verify page content for expected route
    // often you'd use a data-testid or role query, but this is also possible
    expect(container.innerHTML).toMatch("Categories");
    //verify we have checked the token
    expect(jwtDecode).toHaveBeenCalled();
  });
});
