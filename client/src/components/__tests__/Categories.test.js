import React from "react";
import { render } from "./test-utils";
import "@testing-library/jest-dom/extend-expect";

import Categories from "../Categories.js";

describe("<Categories/>", () => {
  it("should render categories if categories loaded in reducer", () => {
    const { container, debug, getByTestId } = render(<Categories />, {
      initialState: {
        allCategory: {
          categories: [
            {
              category: "Bath Bombs",
              createdAt: "2020-09-07T15:51:51.541Z",
              id: "bath-bombs",
              link: "bath-bombs",
            },
          ],
        },
      },
    });
    expect(getByTestId("category-item")).toBeInTheDocument();
  });

  it("should not render categories if categories NOT loaded in reducer", () => {
    const { container, debug, getByTestId } = render(<Categories />, {
      initialState: {
        allCategory: {
          categories: null,
        },
      },
    });
    expect(getByTestId("spinner")).toBeInTheDocument();
  });
});



