import React from "react";
import { fireEvent, render } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";

import CategoryItem from "../CategoryItem.js";

describe('<CategoryItem />', () => {
    it('should redirect user on click', () => {
        const handleRedirect = jest.fn();
        const {getByTestId} = render(<CategoryItem onClick={handleRedirect()} />)
        fireEvent.click(getByTestId('category-item'))
        expect(handleRedirect).toHaveBeenCalled() 
    });
});