import React from "react";
import "@testing-library/jest-dom/extend-expect";
import { BrowserRouter as Router } from "react-router-dom";
import { render } from "../__tests__/test-utils";

import Navbar from "../navigation/Navbar";

describe("Navbar", () => {
  it("should contain logo and navigation icons", () => {
    const { getByText } = render(
      <Router history={history}>
        <Navbar evenClick={open()} />
      </Router>,
      {
        initialState: {
          cart: {
            hidden: true,
            cartItems: [
              {
                category: "Bath Bombs",
                createdAt: "2020-08-15T05:55:13.168Z",
                currency: "GBP",
                id: "blueberry-medium",
                imageUrl:
                  "https://firebasestorage.googleapis.com/v0/b/bath-creations.appspot.com/o/1955050.png?alt=media&token=a8df3fe5-6ba4-43c1-8f02-f50c87e8c3f7",
                link: "blueberry-medium",
                name: "Blueberry",
                price: "2.00",
                quantity: 1,
                size: "Medium",
                tag: "Fruity",
              },
            ],
          },
        },
      }
    );

    expect(getByText(/Menu/)).toBeInTheDocument();
  });

  // it("should have a clickble hamburger icon", () => {
  //   // const isOpen = jest.fn();
  //   // const { getByTestId } = render(<Navbar onClick={isOpen()} />);
  //   // fireEvent.click(getByTestId("navbar-menu"));
  //   // expect(isOpen).toHaveBeenCalled();
  // });

  // it("should navigate to user page if signed in", () => {});
  // it("should navigate to sign in page if not signed in", () => {});
  // it("should navigate to basket page", () => {});
});
