import React from 'react'
import {render} from '@testing-library/react'
import "@testing-library/jest-dom/extend-expect";

import CartItem from '../basket/CartItem';

describe('<CartItem />', () => {
    it('should show cart items', () => {
        const item = {imageUrl: 'test/url', price: '2.00', name: 'Strawberry', quantity: 1, size: 'Medium'}
        const {getByText, debug} = render(<CartItem item={item}/>)
        debug()
        expect(getByText('Strawberry Medium')).toBeInTheDocument()
        expect(getByText('1 x £2.00')).toBeInTheDocument()
    });
})