import Layout from "../layouts/Layout";
import Navbar from "../../components/navigation/Navbar";

import React from "react";
import "@testing-library/jest-dom/extend-expect";
import { BrowserRouter as Router } from "react-router-dom";
import { findAllByRole, fireEvent, getByRole } from "@testing-library/react";
import { createMemoryHistory } from "history";
import { render } from "../__tests__/test-utils";

describe("Layout", () => {
  it("should contain function to handle the showmenu click event", async () => {
    const { getAllByRole, debug } = render(
      <Router history={history}>
        <Navbar handleShowMenu={open} />
        <Layout />
      </Router>,
      {
        initialState: {
          cart: {
            hidden: true,
            cartItems: [
              {
                category: "Bath Bombs",
                createdAt: "2020-08-15T05:55:13.168Z",
                currency: "GBP",
                id: "blueberry-medium",
                imageUrl:
                  "https://firebasestorage.googleapis.com/v0/b/bath-creations.appspot.com/o/1955050.png?alt=media&token=a8df3fe5-6ba4-43c1-8f02-f50c87e8c3f7",
                link: "blueberry-medium",
                name: "Blueberry",
                price: "2.00",
                quantity: 1,
                size: "Medium",
                tag: "Fruity",
              },
            ],
          },
        },
      }
    );

    // const menuButton = getAllByRole("menuitem");
    // fireEvent.click(menuButton[0]);
    // // expect(await handleShowMenu).toHaveBeenCalled();
  });
});
