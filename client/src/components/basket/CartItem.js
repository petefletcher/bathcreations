import React from "react";
import "../../styles/CartItem.scss";

const CartItem = ({ item: { imageUrl, price, name, quantity, size } }) => {
  return (
    <div className="cart-item">
      <div>
        <img src={imageUrl} />
      </div>
      <div>
        <div className="cart-item__name">{`${name} ${size}`}</div>
        <div className="cart-item__price">
          {quantity} x £{price}
        </div>
      </div>
    </div>
  );
};

export default CartItem;
