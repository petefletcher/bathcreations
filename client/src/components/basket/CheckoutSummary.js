import React from "react";
import "../../styles/CheckoutSummary.scss";

import { useSelector } from "react-redux";
import CheckoutItem from "./CheckoutItem";
import StripeCheckoutButtton from "../stripe/StripeButton";
import Button from "../utils/Button";
import paypalImg from "../../img/paypalbtn.png";

import { paypalPaymentsCall } from "../utils/apiCalls";

const CheckoutSummary = () => {
  const cartItems = useSelector((state) => state.cart.cartItems);
  const cartTotal = useSelector((state) => {
    const total = state.cart.cartItems;
    const cartTotal = total.reduce((totalCost, item) => {
      totalCost += parseFloat(item.price).toFixed(2) * item.quantity;
      return Number(totalCost);
    }, 0);
    return cartTotal.toFixed(2);
  });

  const style = { color: "red" };

  const handlePaypalCheckout = () => {
    paypalPaymentsCall(cartItems, cartTotal).then((resp) => {
      window.location.href = resp;
    });
  };

  return (
    <div className="checkout-page">
      <div className="checkout-page__header">
        <div className="checkout-page__header-block">
          <span>Product</span>
        </div>
        <div className="checkout-page__header-block">
          <span>Decription</span>
        </div>
        <div className="checkout-page__header-block">
          <span>Quantity</span>
        </div>
        <div className="checkout-page__header-block">
          <span>Price</span>
        </div>
        <div className="checkout-page__header-block">
          <span>Remove</span>
        </div>
      </div>
      <div className="checkout-page__item-wrapper">
        {cartItems.length > 0 ? (
          cartItems.map((item) => <CheckoutItem key={item.id} item={item} />)
        ) : (
          <div>No items in your cart</div>
        )}
      </div>
      <div className="checkout-page__total">
        <span>Total: £{cartTotal}</span>
      </div>
      <div className="checkout-page__test-warning" style={style}>
        Test card details 4000056655665556
      </div>
      <div className="checkout-page__stripe-pay-button">
        <StripeCheckoutButtton price={cartTotal} items={cartItems} />
      </div>
      <div className="checkout-page__paypal-button">
        <div className="checkout-page__paypal-button--wrapper">
          <img
            src={paypalImg}
            alt="paypal button"
            onClick={handlePaypalCheckout}
          />
        </div>
      </div>
    </div>
  );
};

export default CheckoutSummary;
