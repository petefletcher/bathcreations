import React, { useState, useEffect } from "react";
import {
  startGetTransactionData,
  resetReducer,
  getTransactionData,
} from "../../redux/cart/cart.action";
import { useDispatch, useSelector } from "react-redux";

import Spinner from "../../hoc/LoadingSpinner";
import Aux from "../../hoc/AuxComponent";
import "../../styles/CheckoutComplete.scss";

const CheckoutComplete = (props) => {
  const dispatch = useDispatch();

  const hasTransactionDataLoaded = useSelector((state) => {
    if (state.cart.hasLoaded) {
      return true;
    } else {
      return false;
    }
  });

  const transData = useSelector((state) => {
    return state.cart.transaction;
  });

  const [paypal, setPaypal] = useState(false);
  const [stripe, setStripe] = useState(false);

  useEffect(() => {
    if (window.location.search.length > 0) {
      const transactionId = window.location.search.slice(1);
      dispatch(startGetTransactionData(transactionId));
    }
  }, []);

  const itemMapper = (items) => {
    return items.map((item, i) => {
      return (
        <div key={i} className="checkout-complete__items">
          <div className="checkout_complete__item-name">{item.name}</div>
          <div className="checkout_complete__item-price">{item.price}</div>
          <div className="checkout_complete__item-quantity">
            {item.quantity}
          </div>
        </div>
      );
    });
  };

  const renderTransactionData = () => {
    if (window.location.search.length > 0) {
      return (
        <div className="checkout-complete">
          <div className="checkout-complete__inner-card">
            <div className="checkout-complete__title">
              Thank You For Your Order!
            </div>
            <div className="checkout-complete__payee">{`Hey ${transData.payee[0]}, your purchase details are below :-`}</div>

            <div className="checkout-complete__order-info">{`Order ID: ${window.location.search.slice(
              1
            )},`}</div>
            <div className="checkout-complete__item-wrapper">
              {itemMapper(transData.items.items)}
            </div>
            <div className="checkout-complete__amount"></div>
          </div>
        </div>
      );
    } else {
      const { amount, payee, item_list } = props.history.location.state.success;

      return (
        <div className="checkout-complete">
          <div className="checkout-complete__inner-card">
            <div className="checkout-complete__title">
              Thank You For Your Order!
            </div>
            <div className="checkout-complete__payee">{`Hey ${payee[0]}, your purchase details are below :-`}</div>
            <div className="checkout-complete__order-info">
              {`Order ID: ${payee[1]}`}
            </div>
            <div className="checkout-complete__item-wrapper">
              {itemMapper(item_list.items)}
            </div>
            <div className="checkout-complete__amount"></div>
          </div>
        </div>
      );
    }
  };

  return (
    <Aux>
      {hasTransactionDataLoaded ? (
        renderTransactionData()
      ) : (
        <div className="spinner-wrapper">
          <Spinner />
        </div>
      )}
    </Aux>
  );
};

export default CheckoutComplete;
