import React, { useState, useEffect } from "react";
import { withRouter } from "react-router";

import Button from "../utils/Button";
import CartItem from "./CartItem";

// redux
import { useDispatch, useSelector } from "react-redux";
import { toggleCartDropdown } from "../../redux/cart/cart.action";

import "../../styles/CheckoutSummaryDropdown.scss";

const CheckoutSummaryDropDown = (props) => {
  // redux hooks
  const cartItems = useSelector((state) => state.cart.cartItems);
  const toggleCheckoutDropdown = useSelector((state) => state.cart.hidden);
  const dispatch = useDispatch();

  // react hooks
  // const [orderSummary, setOrderSummary] = useState(props.items);

  const handleGoToCheckout = () => {
    dispatch({
      type: toggleCartDropdown,
    });
    props.history.push("/checkout");
  };

  return (
    <div className="checkout-summary-dropdown">
      <div className="checkout-summary-dropdown__items">
        {cartItems.length > 0 ? (
          cartItems.map((cartItem) => {
            return <CartItem key={cartItem.id} item={cartItem} />;
          })
        ) : (
          <span className="checkout-summary-dropdown__empty-message">
            Your cart is empty
          </span>
        )}
      </div>
      <Button
        textValue="Go To Checkout"
        onClickEvent={() => handleGoToCheckout()}
      />
    </div>
  );
};

export default withRouter(CheckoutSummaryDropDown);
