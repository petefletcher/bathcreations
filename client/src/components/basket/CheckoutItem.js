import React from "react";
import "../../styles/CheckoutItem.scss";

import { useDispatch } from "react-redux";
import { clearItem, removeItem, addItem } from "../../redux/cart/cart.action";

const CheckoutItem = ({ item }) => {
  const dispatch = useDispatch();

  const handleClearItem = () => {
    dispatch({
      type: clearItem,
      payload: item,
    });
  };

  const handleRemoveItem = () => {
    dispatch({
      type: removeItem,
      payload: item,
    });
  };

  const handleAddItem = () => {
    dispatch({
      type: addItem,
      payload: item,
    });
  };

  /** NEED TO CAPS THE SIZE -----------SDFDNMKDOSJNDN */

  return (
    <div className="checkout-items">
      <div className="checkout-items__product">
        <img src={item.imageUrl} />
      </div>
      <div className="checkout-items__name">
        <div>{item.name}</div>
        <div>{item.size}</div>
      </div>
      <div className="checkout-items__quantity">
        <i
          className="las la-2x la-chevron-up checkout-items__chevrons"
          onClick={() => handleAddItem(item)}
        ></i>
        <span className="checkout-items__quantity--text">{item.quantity}</span>
        <i
          className="las la-2x la-chevron-down checkout-items__chevrons"
          onClick={() => handleRemoveItem(item)}
        ></i>
      </div>
      <div className="checkout-items__price">
        <span>{item.price}</span>
      </div>
      <div
        className="checkout-items__remove"
        onClick={() => handleClearItem(item)}
      >
        <i className="las la-2x la-times-circle" />
      </div>
    </div>
  );
};

export default CheckoutItem;
