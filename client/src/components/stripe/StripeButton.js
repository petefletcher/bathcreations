import React from "react";
import StripeCheckout from "react-stripe-checkout";
import { withRouter } from "react-router";
import { useDispatch } from "react-redux";
import { paymentSuccessful } from "../../redux/cart/cart.action";
import { stripePayment } from "./../../components/utils/apiCalls";

import logo from "../../img/bc-logo.jpg";

// stripe wants in pennies
function StripeButton(props) {
  const dispatch = useDispatch();
  const priceForStripe = props.price * 100;
  const publishableKey =
    "pk_test_51H68dnAYQ8nccyGB86ZXvOPEHxbrB7zVWxz4VfPeiT332xO3672tMC7uu6cj5LB4du4vIUciixpjYyOnWHmA8dPO00mA9zjJSz";

  const isTest = process.env.NODE_ENV === "development";

  const onToken = (token) => {
    stripePayment(token, priceForStripe, props.items)
      .then((response) => {
        alert("Payment was succesful");
        dispatch({
          type: paymentSuccessful,
        });
        props.history.push("/checkout-complete", { ...response });
      })
      .catch((error) => {
        alert("There was an issue with your payment");
      });
  };
  return (
    <StripeCheckout
      label="Pay Now"
      name="Bath Creations"
      locale="en"
      currency="GBP"
      billingAddress
      shippingAddress
      image={logo}
      description={`Your total is £${props.price}`}
      amount={priceForStripe}
      panelLabel="Pay Now"
      token={onToken}
      stripeKey={publishableKey}
    />
  );
}

export default withRouter(StripeButton);
