import React, { useEffect, useState } from "react";
import { fetchCategoriesStart } from "../redux/categories/catergories.actions";
import { useDispatch, useSelector } from "react-redux";
import { renderCategories } from "./utils/Utils";
import "../styles/Categories.scss";
import Spinner from "../hoc/LoadingSpinner";

// this will give back the component that we passed in

const Categories = () => {
  const hasCategoriesLoaded = useSelector(
    (state) => !!state.allCategory.categories
  );
  const getCategories = useSelector((state) => state.allCategory.categories);

  const dispatch = useDispatch();

  const tablet = window.innerWidth > 500 && window.innerWidth < 780;

  useEffect(() => {
    if (getCategories === null) {
      dispatch(fetchCategoriesStart());
    }
  }, []);

  return (
    <div className="categories">
      {hasCategoriesLoaded ? (
        renderCategories(getCategories)
      ) : (
        <div className="categories__spinner-wrapper">
          <Spinner />
        </div>
      )}
      {!hasCategoriesLoaded && tablet ? (
        <div className="categories__spinner-wrapper">
          <Spinner />
        </div>
      ) : null}
    </div>
  );
};

export default Categories;
