import React, { useState, useEffect } from "react";
import { useSelector, useDispatch, useReducer } from "react-redux";
import { useHistory } from "react-router-dom";
import "../styles/SignIn.scss";
import Button from "./utils/Button";

import FormInput from "./utils/FormInput";
import {
  googleSignInStart,
  emailSignInStart,
  signInSuccess,
} from "../redux/user/user.action";
import { noError } from "../redux/error/error.action";

/** TODO INVALID / ERROR ON SIGNIN */
const SignIn = () => {
  const [siginDetails, setSignInDetails] = useState({
    signinEmail: "",
    signinPassword: "",
  });

  const currentUser = useSelector((state) => state.user.currentUser);
  const errors = useSelector((state) => state.error.error);

  const history = useHistory();
  const dispatch = useDispatch();

  useEffect(() => {
    if (errors) {
      alert(errors);
      dispatch(noError());
    }
  }, [errors]);

  const handleChangeForm = (event) => {
    const { value, id } = event.target;

    setSignInDetails({
      ...siginDetails,
      [id]: value,
    });
  };

  const submitForm = async (event) => {
    event.preventDefault();
    const { signinEmail, signinPassword } = siginDetails;

    dispatch(emailSignInStart({ signinEmail, signinPassword, isAdmin }));
    setSignInDetails({
      signinEmail: "",
      signinPassword: "",
    });
  };

  let isAdmin = false;

  if (window.location.pathname !== "/signin") {
    isAdmin = true;
  }

  // const startGoogleSignIn = () => {
  //   dispatch(googleSignInStart());
  // };

  return (
    <div className="sign-in">
      {isAdmin ? <h2>Hey Becs!</h2> : <h2>I already have an account</h2>}
      <span className="sign-up__sub-title">
        Sign in with your email and password
      </span>

      <form>
        <FormInput
          name="--sign-in"
          aria-label="email-sign-in-input"
          id="signinEmail"
          type="text"
          placeholder="Email"
          onChange={handleChangeForm}
          value={siginDetails.signinEmail}
          required
        />
        <FormInput
          name="--sign-in"
          aria-label="password-sign-in-input"
          id="signinPassword"
          type="password"
          placeholder="Password"
          onChange={handleChangeForm}
          value={siginDetails.signinPassword}
          required
        />
        <div className="sign-in__button-wrapper">
          <Button
            id="sigin-button"
            textValue="Sign In"
            onClickEvent={submitForm}
            helperText={null}
          />
          {/* <Button
            id="google-button"
            textValue="Sign in With Google "
            onClickEvent={startGoogleSignIn}
            helperText={null}
          /> */}
        </div>
      </form>
    </div>
  );
};

export default SignIn;
