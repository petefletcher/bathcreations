import React, { useEffect } from "react";
import { useDispatch } from "react-redux";

export default function Category({ match }) {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch({ type: clearTransactionData });
  }, []);

  // can take out title format once we have the data
  const { id } = match.params;

  let title;
  let formattedTitle;
  if (id.includes("-")) {
    title = id.split("-");
    formattedTitle = title
      .map((element) => {
        let firstChar = element.charAt(0).toUpperCase();
        let EndOfTitle = element.substring(1);
        return `${firstChar}${EndOfTitle}`;
      })
      .join(" ");
  } else {
    let firstChar = id.charAt(0).toUpperCase();
    let EndOfTitle = id.substring(1);

    formattedTitle = `${firstChar}${EndOfTitle}`;
  }
  return <div>{formattedTitle}</div>;
}
