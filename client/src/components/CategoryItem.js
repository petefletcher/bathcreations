import React from "react";
import "../styles/CategoryItem.scss";

function CategoryItem({ category, link, image, history, match, ...props }) {
  return (
    <div
      className={`category-item${image}`}
      data-testid="category-item"
      id={link}
      onClick={() => (window.location = `${link}`)}
    >
      <div className={`category-item__content`}>
        <h1 className="catergory-item__title">{category}</h1>
        <span className="category-item__subtitle">Shop Now</span>
      </div>
    </div>
  );
}

// withRouter is a HOC giving access to the router pros withou passing all down
export default CategoryItem;
