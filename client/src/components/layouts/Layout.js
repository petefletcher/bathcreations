import React, { useState } from "react";
import { withRouter } from "react-router";

//redux
import { useDispatch, useSelector } from "react-redux";
import { toggleCartDropdown } from "../../redux/cart/cart.action";

import { signOutStart } from "../../redux/user/user.action";

import Aux from "../../hoc/AuxComponent";

import Navbar from "../navigation/Navbar";
import SideDrawer from "../navigation/SideDrawer/SideDrawer";
import Footer from "../layouts/Footer";
import SearchBox from "../utils/SearchBox";
import CheckoutSummaryDropDown from "../basket/CheckoutSummaryDropDown";

const Layout = (props) => {
  const toggleCheckoutDropdown = useSelector((state) => state.cart.hidden);
  const dispatch = useDispatch();

  const [showSideDrawer, setShowSideDrawer] = useState(false);
  const [showMenu, setShowMenu] = useState(false);
  const [showSearch, setShowSearch] = useState(false);
  const [showModal, setShowModal] = useState(false);

  const handleShowMenu = () => {
    setShowMenu(true);
    setShowSideDrawer(true);
  };

  const handleShowSearch = () => {
    setShowSearch(!showSearch);
  };

  const handleShowModal = () => {
    setShowModal(!showModal);
  };

  const sideDrawerClosedHandler = () => {
    setShowSideDrawer(false);
  };

  const handleSignOut = () => {
    dispatch(signOutStart());
    window.location = "/";
  };

  const handleToggleCheckoutDropdown = () => {
    dispatch({
      type: toggleCartDropdown,
    });
  };

  return (
    <Aux>
      <Navbar
        open={handleShowMenu}
        search={handleShowSearch}
        signOut={handleSignOut}
        toggleCheckout={handleToggleCheckoutDropdown}
      />
      {/* <SearchBox open={showSearch} /> */}
      <SideDrawer open={showSideDrawer} closed={sideDrawerClosedHandler} />
      <main className="main">{props.children}</main>
      {!toggleCheckoutDropdown ? <CheckoutSummaryDropDown /> : null}
      <Footer />
    </Aux>
  );
};

export default Layout;
