import React, { useEffect, useState } from "react";
import "../styles/App.scss";

import {
  checkUserSession,
  fetchUserCredsSuccess,
} from "./../redux/user/user.action";

import { Switch, Route } from "react-router-dom";
import jwtDecode from "jwt-decode";

// redux
import { useSelector, useDispatch } from "react-redux";
import { signOutStart } from "../redux/user/user.action";

//layouts
import Layout from "../components/layouts/Layout";
import HomePage from "../containers/HomePage";

//pages
import AdminSignIn from "./pages/AdminSignIn";
import BathBombs from "./pages/BathBombs";
import Categories from "./Categories";
import Fruity from "./pages/Fruity";
import FloorFresheners from "./pages/FloorFresheners";
import GiftSets from "./pages/GiftSets";
import ManageCategories from "./pages/ManageCategories";
import ManageProducts from "./pages/ManageProducts";
import Men from "./pages/Men";
import Others from "./pages/Others";
import SignInSignUp from "./pages/SignIn-SignUp";
import Sponges from "./pages/Sponges";
import Transactions from "./pages/Transactions";
import Women from "./pages/Women";
import WaxMelts from "./pages/WaxMelts";

//checkout / payment
import Checkout from "../containers/Checkout";
import CheckoutComplete from "./basket/CheckoutComplete";

// utils
import { fetchProductsStart } from "../redux/products/products.action";

import { fetchUserCredsFailure } from "../redux/user/user.action";

const App = () => {
  const dispatch = useDispatch();

  let authenticated;

  const token = localStorage.bcToken;
  if (token) {
    const decodedToken = jwtDecode(token);

    if (decodedToken.exp * 1000 < Date.now()) {
      authenticated = false;
      dispatch(signOutStart());
    } else {
      authenticated = true;
    }
  }

  if (authenticated === true) {
    const decodedToken = jwtDecode(token);
    if (decodedToken.admin === true) {
      dispatch(fetchUserCredsSuccess());
    } else {
      dispatch(fetchUserCredsFailure());
    }
  }

  useEffect(() => {
    dispatch(fetchProductsStart());
  }, []);

  return (
    <div className="App">
      <Layout>
        <Switch>
          <Route exact path="/categories" component={Categories} />
          <Route exact path="/" component={HomePage} />
          <Route exact path="/admin-bath-creations" component={AdminSignIn} />

          <Route exact path="/bath-bombs" component={BathBombs} />
          <Route exact path="/sponges" component={Sponges} />
          <Route exact path="/gift-sets" component={GiftSets} />
          <Route exact path="/wax-melts" component={WaxMelts} />
          <Route exact path="/floorFresheners" component={FloorFresheners} />
          <Route exact path="/fruity" component={Fruity} />
          <Route exact path="/women" component={Women} />
          <Route exact path="/men" component={Men} />
          <Route exact path="/manage-products" component={ManageProducts} />
          <Route exact path="/manage-categories" component={ManageCategories} />
          <Route exact path="/manage-transactions" component={Transactions} />
          <Route exact path="/others" component={Others} />
          {/* <Route
            exact
            path="/signin"
            render={() => (authenticated ? <HomePage /> : <SignInSignUp />)}
          /> */}

          <Route exact path="/checkout" component={Checkout} />
          <Route exact path="/checkout-complete" component={CheckoutComplete} />
        </Switch>
      </Layout>
    </div>
  );
};

export default App;
