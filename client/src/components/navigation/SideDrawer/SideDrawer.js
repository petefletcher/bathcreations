import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { withRouter } from "react-router-dom";

import { fetchCategoriesStart } from "../../../redux/categories/catergories.actions";

import Logo from "../../../logo/Logo";
import NavigationItems from "../navigationItems/NavigationItems";
import NavHeading from "../navigationItems/navigationItem/NavigationHeading";
import Backdrop from "../../../ui/Backdrop";
import Aux from "../../../hoc/AuxComponent";
import Spinner from "../../../hoc/LoadingSpinner";
import "../../../styles/SideDrawer.scss";

const SideDrawer = (props) => {
  // TODO make 100% and scroll
  const getCategories = useSelector((state) => state.allCategory.categories);
  const hasUser = useSelector((state) => state.user.claim);

  const dispatch = useDispatch();

  const hasCategoriesLoaded = useSelector(
    (state) => !!state.allCategory.categories
  );

  useEffect(() => {
    if (getCategories === null) {
      dispatch(fetchCategoriesStart());
    }
  }, []);

  // conditionally attach classess
  let attachedClasses = `side-drawer side-drawer__close`;
  if (props.open) {
    attachedClasses = `side-drawer side-drawer__open`;
  }

  return (
    <Aux>
      <Backdrop show={props.open} clicked={props.closed} />
      <div className={attachedClasses}>
        <div className="side-drawer__logo-container">
          <Logo
            jc="center"
            borderBottom="1px solid #dda0dd;"
            clicked={props.closed}
          />
        </div>
        <div className="side-drawer__categories-container">
          <div className="side-drawer__categories-heading">Categories</div>
          <nav>
            <NavigationItems className="__side-drawer-categories">
              {hasCategoriesLoaded ? (
                getCategories.map((category, i) => {
                  return (
                    <NavHeading
                      key={`${category}-${i}`}
                      class={`${category.link}-${i}`}
                      location={"__side-drawer"}
                      role="heading"
                      aria={`${category.category}`}
                      eventClick={props.closed}
                      redirect={() => {
                        window.location = `${category.link}`;
                      }}
                      style="text-decoration:none"
                    />
                  );
                })
              ) : (
                <div className="categories__spinner-wrapper">
                  <Spinner />
                </div>
              )}
            </NavigationItems>
          </nav>
        </div>
        {hasUser ? (
          <div className="side-drawer__admin-interactions">
            <div className="side-drawer__admin-header">Admin</div>
            <div
              className="side-drawer__crud-admin"
              onClick={() => {
                window.location = "/manage-categories";
                props.closed();
              }}
            >
              Categories
            </div>
            <div
              className="side-drawer__crud-admin"
              onClick={() => {
                window.location = "/manage-products";
                props.closed();
              }}
            >
              Products
            </div>
            <div
              className="side-drawer__crud-admin"
              onClick={() => {
                window.location = "/manage-transactions";
                props.closed();
              }}
            >
              Transactions
            </div>
          </div>
        ) : (
          <div className="empty-div"></div>
        )}
        <div className="side-drawer__footer-menus">Bath Creations 2020</div>
      </div>
    </Aux>
  );
};

export default withRouter(SideDrawer);
