import React from "react";

import NavItem from "./navigationItem/NavigationItem";
import "../../../styles/NavigationItems.scss";

const NavigationItems = (props) => {
  return (
    <ul className={`navigation-items${props.className}`}>{props.children}</ul>
  );
};

export default NavigationItems;
