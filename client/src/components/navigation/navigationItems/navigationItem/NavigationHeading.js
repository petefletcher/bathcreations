import React from "react";
import { Link } from "react-router-dom";
import "../../../../styles/NavigationHeading.scss";

const NavigationHeading = (props) => {
  return (
    <li
      className={
        props.location
          ? `navigation-heading${props.location}`
          : "navigation-heading"
      }
    >
      {props.link ? (
        <Link to={props.link}>
          <div className="navigation-heading__component">
            <div className={props.class} role="heading" aria-label={props.aria}>
              {props.aria}
            </div>
          </div>
        </Link>
      ) : (
        <div
          className="navigation-heading__component"
          onClick={
            props.redirect
              ? (event) => {
                  props.eventClick(event);
                  props.redirect();
                }
              : (event) => props.eventClick(event)
          }
        >
          <div className={props.class} role="heading" aria-label={props.aria}>
            {props.aria}
          </div>
        </div>
      )}
    </li>
  );
};

export default NavigationHeading;
