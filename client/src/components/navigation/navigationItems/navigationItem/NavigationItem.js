import React from "react";
import { Link } from "react-router-dom";
import "../../../../styles/NavigationItem.scss";

const NavigationItem = (props) => {
  return (
    <li className="navigation-item" role="menuitem">
      {/* This could be anything, an 'a' tag keeping it extracted makes it reuseable */}
      {props.link ? (
        <Link to={props.link}>
          <div className="navigation-item__component">
            <i className={props.class} role="img" aria-label={props.aria}></i>
          </div>
        </Link>
      ) : (
        <div
          className="navigation-item__component"
          onClick={(event) => props.eventClick(event)}
        >
          <i className={props.class} role="img" aria-label={props.aria}></i>
        </div>
      )}
    </li>
  );
};

export default NavigationItem;
