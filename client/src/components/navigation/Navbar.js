import React, { useState } from "react";
import "../../styles/Navbar.scss";

// react-redux
import { useSelector, useDispatch } from "react-redux";

// components
import Logo from "../../logo/Logo";
import NavigationItems from "../navigation/navigationItems/NavigationItems";
import NavItem from "../navigation/navigationItems/navigationItem/NavigationItem";
import NavHeading from "../navigation/navigationItems/navigationItem/NavigationHeading";

const Navbar = (props) => {
  //TODO stickybar
  const user = useSelector((state) => state.user.currentUser);
  const toggleCheckoutDropdown = useSelector((state) => state.cart.hidden);

  const currentItemCount = useSelector((state) => {
    let items = state.cart.cartItems;
    const totalItemCount = items.reduce((acc, cartItem) => {
      return acc + cartItem.quantity;
    }, 0);
    return totalItemCount;
  });

  const cartItems = useSelector((state) => state.cart.cartItems);
  const dispatch = useDispatch();

  const [itemCount, setCurrentItemCount] = useState(currentItemCount);

  let iconSize = "";
  const mobile = window.innerWidth < 500;
  let hasUser = false;

  if (user === null) {
    hasUser = false;
  } else {
    hasUser = true;
  }

  if (mobile) {
    iconSize = "la-2x";
  } else {
    iconSize = "la-3x";
  }

  return (
    <header className="navbar" role="menu">
      <div className="navbar__nav-items-left">
        <NavigationItems className="__navbar">
          {mobile ? (
            <NavItem
              class={`las ${iconSize} la-bars`}
              role="img"
              aria="hamburger"
              eventClick={props.open}
            />
          ) : (
            <NavHeading
              class={`menu-heading`}
              role="heading"
              aria="Menu"
              eventClick={props.open}
            />
          )}

          {/* {mobile ? (
            <NavItem
              class={`las ${iconSize} la-search`}
              role="img"
              aria="magnifiying-glass"
              //the below needs to be search bar
              eventClick={props.search}
            />
          ) : (
            <NavHeading
              class={`menu-heading`}
              role="heading"
              aria="Search"
              eventClick={props.search}
            />
          )} */}
        </NavigationItems>
      </div>

      <Logo jc="center" clicked={() => (window.location = "/")} />

      <div className="navbar__nav-items-right">
        <NavigationItems className="__navbar">
          {!hasUser ? (
            <div></div>
          ) : (
            <NavItem
              class={`las ${iconSize} la-sign-out-alt`}
              aria="sign-out-icon"
              eventClick={props.signOut}
            />
          )}

          <NavItem
            class={`las ${iconSize} la-shopping-basket`}
            aria="basket-icon"
            eventClick={props.toggleCheckout}
          />
        </NavigationItems>
        <div className="navbar__cart-number">{currentItemCount}</div>
      </div>
    </header>
  );
};

export default Navbar;
