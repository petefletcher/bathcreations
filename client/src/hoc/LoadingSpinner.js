import React from "react";
import "../styles/LoadingSpinner.scss";

const Spinner = () => {
  return <div className="loading-spinner" data-testid="spinner"></div>;
};

export default Spinner;
