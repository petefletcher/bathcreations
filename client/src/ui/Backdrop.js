import React from "react";

import "../styles/Backdrop.scss";

function Backdrop(props) {
  return props.show ? (
    <div
      data-testid="backdrop"
      className="backdrop"
      onClick={props.clicked}
    ></div>
  ) : null;
}

export default Backdrop;
