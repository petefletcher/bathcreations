import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter } from "react-router-dom";
import { PersistGate } from "redux-persist/integration/react";

// components
import App from "./components/App";
import "../dist/index.html";

// redux
import { Provider } from "react-redux";
import { store, persistor } from "./redux/store";

const app = (
  <Provider store={store}>
    <BrowserRouter>
      <PersistGate persistor={persistor}>
        <App />
      </PersistGate>
    </BrowserRouter>
  </Provider>
);
const appElement = document.getElementById("app");

ReactDOM.render(app, appElement);
