import { all, call } from "redux-saga/effects";
import { onFetchProductsStart } from "./products/products.sagas";
import { userSagas } from "./user/user.sagas";
import { cartSagas } from "./cart/cart.sagas";
import { categoriesSagas } from "./categories/categories.sagas";

export default function* rootSaga() {
  yield all([
    call(onFetchProductsStart),
    call(userSagas),
    call(cartSagas),
    call(categoriesSagas),
  ]);
}
