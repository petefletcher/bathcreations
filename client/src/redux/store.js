import { createStore, applyMiddleware } from "redux";
import { persistStore } from "redux-persist";
import logger from "redux-logger";
import createSagaMiddleWare from "redux-saga";

import rootReducer from "./root-reducer";
import rootSaga from "./root-saga";

const sageMiddleWare = createSagaMiddleWare();
const middlewares = [sageMiddleWare];

export const store = createStore(rootReducer, applyMiddleware(...middlewares));

sageMiddleWare.run(rootSaga);

export const persistor = persistStore(store); // acts like localstorage

export default { store, persistor };
