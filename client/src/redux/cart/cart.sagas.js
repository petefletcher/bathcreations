import { all, call, takeLatest, put } from "redux-saga/effects";
import {
  getCompleteTransactionPaypal,
  getCompleteTransactionStripe,
  getAllTransactions,
} from "../../components/utils/apiCalls";

import UserActionTypes from "../user/user.types";
import {
  clearCart,
  getTransactionData,
  clearTransactionData,
  fetchTransactionsSuccess,
  fetchTransactionsFailure,
} from "./cart.action";
import CartActionTypes from "./cart.types";

export function* clearCartOnSignOut() {
  yield put(clearCart());
}

export function* onSignOutSuccess() {
  yield takeLatest(UserActionTypes.SIGN_OUT_SUCCESS, clearCartOnSignOut);
}

export function* onPaymentSucess() {
  yield (CartActionTypes.PAYMENT_SUCCESSFUL, clearCartOnSignOut);
}

export function* getTransactionDataAsync({ payload }) {
  yield put(clearCart());
  yield put(clearTransactionData());

  if (payload.includes("PAYID")) {
    try {
      const transaction = yield getCompleteTransactionPaypal(payload);
      yield put(getTransactionData(transaction));
    } catch (error) {
      console.log(error);
    }
  } else {
    try {
      const transaction = yield getCompleteTransactionStripe(payload);
      yield put(getTransactionData(transaction));
    } catch (error) {
      console.log(error);
    }
  }
}

export function* onGetTransactionDataStart() {
  yield takeLatest(
    CartActionTypes.START_GET_TRANSACTION,
    getTransactionDataAsync
  );
}

export function* fetchTransactionsAsync() {
  try {
    const allTransactions = yield getAllTransactions();

    // put in sagas is like dispatch
    yield put(fetchTransactionsSuccess(allTransactions));
  } catch (error) {
    yield put(fetchTransactionsFailure, error.message);
  }
}

export function* onFetchTransactionsStart() {
  // yield control of the libraries back to the store
  yield takeLatest(
    CartActionTypes.FETCH_TRANSACTIONS_START,
    fetchTransactionsAsync
  );
}

export function* cartSagas() {
  yield all([
    call(onSignOutSuccess),
    call(onGetTransactionDataStart),
    call(onFetchTransactionsStart),
  ]);
}
