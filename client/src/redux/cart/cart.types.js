const CartActionTypes = {
  CLEAR_CART: "CLEAR_CART",
  PAYMENT_SUCCESSFUL: "PAYMENT_SUCCESSFUL",
  SET_ITEM_COUNT: "SET_ITEM_COUNT",
  REMOVE_ITEM: "REMOVE_ITEM",
  CLEAR_ITEM: "CLEAR_ITEM",
  ADD_ITEM: "ADD_ITEM",
  TOGGLE_CART_DROPDOWN: "TOGGLE_CART_DROPDOWN",
  GET_TRANSACTION: "GET_TRANSACTION",
  START_GET_TRANSACTION: "START_GET_TRANSACTION",
  CLEAR_TRANSACTION_DATA: "CLEAR_TRANSACTION_DATA",
  CLEAR_TRANSACTION: "CLEAR_TRANSACTION",
  RESET: "RESET",
  FETCH_TRANSACTIONS_START: "FETCH_TRANSACTIONS_START",
  FETCH_TRANSACTIONS_SUCCESS: "FETCH_TRANSACTIONS_SUCCESS",
  FETCH_TRANSACTIONS_FAILURE: "FETCH_TRANSACTIONS_FAILURE",
};

export default CartActionTypes;
