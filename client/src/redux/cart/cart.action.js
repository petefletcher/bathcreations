import CartActionTypes from "./cart.types";

export const toggleCartDropdown = () => {
  return {
    type: CartActionTypes.TOGGLE_CART_DROPDOWN,
  };
};

export const addItem = (item) => {
  return {
    type: CartActionTypes.CLEAR_CART,
    payload: item,
  };
};

export const clearItem = (item) => ({
  type: CartActionTypes.CLEAR_ITEM,
  payload: item,
});

export const removeItem = (item) => ({
  type: CartActionTypes.REMOVE_ITEM,
  payload: item,
});

export const setItemCount = (count) => {
  return {
    type: CartActionTypes.SET_ITEM_COUNT,
    payload: count,
  };
};

export const paymentSuccessful = () => {
  return { type: CartActionTypes.PAYMENT_SUCCESSFUL };
};

export const clearCart = () => ({
  type: CartActionTypes.CLEAR_CART,
});

export const clearTransactionData = () => ({
  type: CartActionTypes.CLEAR_TRANSACTION_DATA,
});

export const startGetTransactionData = (transactionId) => {
  return {
    type: CartActionTypes.START_GET_TRANSACTION,
    payload: transactionId,
  };
};

export const getTransactionData = (transaction) => {
  return {
    type: CartActionTypes.GET_TRANSACTION,
    payload: transaction,
  };
};

export const fetchTransactionsStart = () => {
  return {
    type: CartActionTypes.FETCH_TRANSACTIONS_START,
  };
};

export const fetchTransactionsSuccess = (allTransactions) => {
  return {
    type: CartActionTypes.FETCH_TRANSACTIONS_SUCCESS,
    payload: allTransactions,
  };
};

export const fetchTransactionsFailure = (errorMessage) => {
  return {
    type: CategoryActionTypes.FETCH_TRANSACTIONS_FAILURE,
    payload: errorMessage,
  };
};

export const clearTransaction = () => {
  return {
    type: CartActionTypes.CLEAR_TRANSACTION,
  };
};

export const resetReducer = () => {
  return {
    type: CartActionTypes.RESET,
  };
};
