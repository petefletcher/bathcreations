import CartActionTypes from "./cart.types";
import { addItemToCart, removeItemFromCart } from "./cart.utils";

import {
  addItem,
  toggleCartDropdown,
  clearItem,
  removeItem,
  setItemCount,
  paymentSuccessful,
} from "./cart.action";

const INITIAL_STATE = {
  hidden: true,
  cartItems: [],
  itemCount: 0,
  transaction: {},
  transactions: [],
  hasLoaded: false,
};

const cartReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case toggleCartDropdown:
      return {
        ...state,
        hidden: !state.hidden,
      };
    case addItem:
      return {
        ...state,
        cartItems: addItemToCart(state.cartItems, action.payload),
      };
    case clearItem:
      return {
        ...state,
        cartItems: state.cartItems.filter(
          (cartItem) => cartItem.id !== action.payload.id
        ),
      };
    case removeItem:
      return {
        ...state,
        cartItems: removeItemFromCart(state.cartItems, action.payload),
      };
    case setItemCount:
      return {
        itemCount: action.payload,
      };
    case paymentSuccessful:
      return {
        ...state,
        cartItems: [],
      };
    case CartActionTypes.CLEAR_CART:
      return {
        ...state,
        cartItems: [],
        hasLoaded: false,
      };

    case CartActionTypes.GET_TRANSACTION:
      return {
        ...state,
        transaction: Object.assign({}, action.payload),
        hasLoaded: true,
      };

    case CartActionTypes.FETCH_TRANSACTIONS_START:
      return {
        ...state,
        isFetching: true,
        hasLoaded: false,
      };

    case CartActionTypes.FETCH_TRANSACTIONS_SUCCESS:
      return {
        ...state,
        isFetching: false,
        transactions: action.payload,
        hasLoaded: true,
      };

    case CartActionTypes.FETCH_TRANSACTIONS_FAILURE:
      return {
        ...state,
        isFetching: false,
        hasLoaded: false,
        errorMessage: action.payload,
      };

    case CartActionTypes.CLEAR_TRANSACTION_DATA:
      return {
        ...state,
        transaction: {},
        hasLoaded: false,
      };

    case CartActionTypes.CLEAR_TRANSACTION:
      return {
        ...state,
        hasLoaded: false,
      };

    case CartActionTypes.RESET:
      return INITIAL_STATE;

    default:
      return state;
  }
};

export default cartReducer;
