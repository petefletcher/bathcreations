import ProductActionTypes from "./products.types";

import {
  firestore,
  convertProductsSnapshotToObject,
} from "../../firebase/firebase.utils";

export const showTheModal = () => {
  return {
    type: ProductActionTypes.SHOW_THE_MODAL,
  };
};

export const hideTheModal = () => {
  return {
    type: ProductActionTypes.HIDE_THE_MODAL,
  };
};

export const addAllProducts = (products) => {
  return {
    type: ProductActionTypes.ADD_ALL_PRODUCTS,
    payload: products,
  };
};

export const fetchProductsStart = () => {
  return {
    type: ProductActionTypes.FETCH_PRODUCTS_START,
  };
};

export const fetchProductsSuccess = (allProducts) => {
  return {
    type: ProductActionTypes.FETCH_PRODUCTS_SUCCESS,
    payload: allProducts,
  };
};

export const fetchProductsFailure = (errorMessage) => {
  return {
    type: ProductActionTypes.FETCH_PRODUCTS_FAILURE,
    payload: errorMessage,
  };
};

export const fetchProductsStartAsync = () => {
  return (dispatch) => {
    // get the data from firestore
    const collectionRef = firestore.collection("products");
    dispatch(fetchProductsStart());
    // whenever the db is updated or visited for the 1st time it will render new data
    collectionRef
      .get()
      .then((snapshot) => {
        let allProducts = convertProductsSnapshotToObject(snapshot);

        dispatch(fetchProductsSuccess(allProducts));
      })
      .catch((error) => dispatch(fetchProductsFailure(error.message)));
  };
};

// thunks are action creaters that returns a fn to get the dispatch
// action is just a javascript object
// if redux thunk is enabled any time we dispatch a fn insteaad ofan object, the middleware will call
// that dispatch method for it first argument
// this is so we can us async code inside it
/** Thunk as middleware
 * actionOrThunk =>
  typeof actionOrThunk === 'function'
    ? actionOrThunk(dispatch, getState)
    : passAlong(actionOrThunk);
 */
