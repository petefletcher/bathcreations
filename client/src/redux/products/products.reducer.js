import ProductActionTypes from "./products.types.js";

const INITIAL_STATE = {
  products: null,
  isFetching: false,
  errorMessage: undefined,
  modalVisible: false,
};

const productsReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case ProductActionTypes.FETCH_PRODUCTS_START:
      return {
        ...state,
        isFetching: true,
      };

    case ProductActionTypes.FETCH_PRODUCTS_SUCCESS:
      return {
        ...state,
        isFetching: false,
        products: action.payload,
      };

    case ProductActionTypes.FETCH_PRODUCTS_FAILURE:
      return {
        ...state,
        isFetching: false,
        errorMessage: action.payload,
      };

    case ProductActionTypes.SHOW_THE_MODAL:
      return {
        ...state,
        modalVisible: true,
      };

    case ProductActionTypes.HIDE_THE_MODAL:
      return {
        ...state,
        modalVisible: false,
      };
    default:
      return state;
  }
};

export default productsReducer;
