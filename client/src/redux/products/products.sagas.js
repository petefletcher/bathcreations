import { takeLatest, call, put } from "redux-saga/effects";
import ProductActionTypes from "./products.types";
import { getProducts } from "./../../components/utils/apiCalls";

import { fetchProductsSuccess, fetchProductsFailure } from "./products.action";

export function* fetchProductsAsync() {
  try {
    const allProducts = yield getProducts();

    // put in sagas is like dispatch
    yield put(fetchProductsSuccess(allProducts));
  } catch (error) {
    yield put(fetchProductsFailure, error.message);
  }
}

export function* onFetchProductsStart() {
  // yield control of the libraries back to the store
  yield takeLatest(ProductActionTypes.FETCH_PRODUCTS_START, fetchProductsAsync);
}
