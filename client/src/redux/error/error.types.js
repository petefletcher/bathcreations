const ErrorActionTypes = {
  ERROR_HAS_OCCURRED: "ERROR_HAS_OCCURRED",
  NO_ERROR: "NO_ERROR",
};

export default ErrorActionTypes;
