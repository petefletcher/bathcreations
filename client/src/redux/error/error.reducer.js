// import the types
import ErrorActionTypes from "./error.types";

// set the state
const INITIAL_STATE = {
  error: null,
};

const errorReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case ErrorActionTypes.ERROR_HAS_OCCURRED:
      return {
        ...state,
        error: action.payload,
      };

    case ErrorActionTypes.NO_ERROR:
      return {
        ...state,
        error: null,
      };
    default:
      return state;
  }
};

export default errorReducer;
