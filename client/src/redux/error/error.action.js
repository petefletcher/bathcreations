import ErrorActionTypes from "./error.types";

export const errorHasOccured = (error) => {
  return {
    type: ErrorActionTypes.ERROR_HAS_OCCURRED,
    payload: error,
  };
};

export const noError = () => {
  return {
    type: ErrorActionTypes.NO_ERROR,
  };
};
