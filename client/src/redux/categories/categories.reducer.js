import { updateCategories } from "./catergories.actions";
import CategoryActionTypes from "./categories.types";

const INTIALSTATE = {
  categories: null,
  isFetching: false,
  errorMessage: undefined,
};

const categoriesReducer = (state = INTIALSTATE, action) => {
  switch (action.type) {
    case CategoryActionTypes.FETCH_CATEGORIES_START:
      return {
        ...state,
        isFetching: true,
      };

    case CategoryActionTypes.FETCH_CATEGORIES_SUCCESS:
      return {
        ...state,
        isFetching: false,
        categories: action.payload,
      };

    case CategoryActionTypes.FETCH_CATEGORIES_FAILURE:
      return {
        ...state,
        isFetching: false,
        errorMessage: action.payload,
      };
    case updateCategories:
      return [state, action.payload];
    default:
      return state;
  }
};

export default categoriesReducer;
