import { takeLatest, call, put, all } from "redux-saga/effects";
import CategoriesActionTypes from "./categories.types";
import { getCategories } from "./../../components/utils/apiCalls";

import {
  fetchCategoriesSuccess,
  fetchCategoriesFailure,
} from "./catergories.actions";

export function* fetchCategoriesAsync() {
  try {
    const allCategories = yield getCategories();

    // put in sagas is like dispatch
    yield put(fetchCategoriesSuccess(allCategories));
  } catch (error) {
    yield put(fetchCategoriesFailure, error.message);
  }
}

export function* onFetchCategoriesStart() {
  // yield control of the libraries back to the store
  yield takeLatest(
    CategoriesActionTypes.FETCH_CATEGORIES_START,
    fetchCategoriesAsync
  );
}

export function* categoriesSagas() {
  yield all([call(onFetchCategoriesStart)]);
}
