export const selectCategory = (categories, category) => {
  let categorySelector = categories.find(
    (products) => products.category === category
  );

  return categorySelector;
};
