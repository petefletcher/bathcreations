import CategoryActionTypes from "./categories.types";

export const fetchCategoriesStart = () => {
  return {
    type: CategoryActionTypes.FETCH_CATEGORIES_START,
  };
};

export const fetchCategoriesSuccess = (allCategories) => {
  return {
    type: CategoryActionTypes.FETCH_CATEGORIES_SUCCESS,
    payload: allCategories,
  };
};

export const fetchCategoriesFailure = (errorMessage) => {
  return {
    type: CategoryActionTypes.FETCH_CATEGORIES_FAILURE,
    payload: errorMessage,
  };
};

export const updateCategories = (allCategories) => {
  return {
    type: CategoryActionTypes.ADD_ALL_CATEGORIES,
    payload: allCategories,
  };
};
