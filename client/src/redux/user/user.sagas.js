import { takeLatest, put, all, call } from "redux-saga/effects";
import UserActionTypes from "./user.types";
import {
  signInSuccess,
  signInFailure,
  signOutSuccess,
  signOutFailure,
  signUpSuccess,
  fetchUserCredsFailure,
} from "./user.action";
import { errorHasOccured, noError } from "../error/error.action";
import {
  auth,
  googleProvider,
  createUserProfileDocument,
  getCurrentUser,
} from "../../firebase/firebase.utils";
import {
  signInAdminEmailAndPassword,
  getSession,
} from "../../components/utils/apiCalls";

/** most of the applications logic has now been moved out to the sagas */

export function* getSnapshotFromUserAuth(userAuth, additionalInfo) {
  try {
    const userRef = yield call(
      createUserProfileDocument,
      userAuth,
      additionalInfo
    );
    const userSnapshot = yield userRef.get();
    yield put(signInSuccess({ id: userSnapshot.id, ...userSnapshot.data() }));
  } catch (error) {
    yield put(signInFailure(error));
  }
}

export function* signInWithGoogle() {
  try {
    const { user } = yield auth.signInWithPopup(googleProvider);
    yield getSnapshotFromUserAuth(user);
  } catch (error) {
    yield put(signInFailure(error));
  }
}

export function* signInWithEmail({
  payload: { signinEmail, signinPassword, isAdmin },
}) {
  let signInAdmin = null;
  try {
    if (isAdmin) {
      signInAdmin = yield signInAdminEmailAndPassword(
        signinEmail,
        signinPassword
      );

      try {
        localStorage.setItem("bcToken", signInAdmin.token);
        const currentSession = yield getSession(signInAdmin.token);
        yield put(signInSuccess(currentSession));
        window.location.href = "/";
      } catch (error) {
        alert(`There was a problem when signing in, please try again`);
        yield put(signInFailure(error));
      }
    } else {
      // this would be normal sign in
    }
  } catch (error) {
    yield put(errorHasOccured(error.response.data.error));
  }
}

export function* onGoogleSignInStart() {
  yield takeLatest(UserActionTypes.GOOGLE_SIGN_IN_START, signInWithGoogle);
}

export function* onEmailSignInStart() {
  yield takeLatest(UserActionTypes.EMAIL_SIGN_IN_START, signInWithEmail);
}

/** User Session */
export function* isUserAuthenticated() {
  try {
    const userAuth = yield getCurrentUser();
    if (!userAuth) return;
    yield getSnapshotFromUserAuth(userAuth);
  } catch (error) {
    yield put(signInFailure(error));
  }
}

export function* onCheckUserSession() {
  yield takeLatest(UserActionTypes.CHECK_USER_SESSION, isUserAuthenticated);
}

export function* signOutApp() {
  try {
    yield auth.signOut();
    localStorage.removeItem("bcToken");
    yield put(fetchUserCredsFailure());
    yield put(signOutSuccess());
  } catch (error) {
    yield put(signOutFailure(error));
  }
}

export function* onSignOutStart() {
  yield takeLatest(UserActionTypes.SIGN_OUT_START, signOutApp);
}

export function* signUp({ payload: { email, password, displayName } }) {
  try {
    const { user } = yield auth.createUserWithEmailAndPassword(email, password);

    yield put(signUpSuccess({ user, additionalInfo: { displayName } }));
  } catch (error) {
    yield put(signOutFailure(error));
  }
}
export function* signInAfterSignUp({ payload: { user, additionalInfo } }) {
  yield getSnapshotFromUserAuth(user, additionalInfo);
}

export function* onSignUpSuccess() {
  yield takeLatest(UserActionTypes.SIGN_UP_SUCCESS, signInAfterSignUp);
}

export function* onSignUpStart() {
  yield takeLatest(UserActionTypes.SIGN_UP_START, signUp);
}

export function* userSagas() {
  yield all([
    call(onGoogleSignInStart),
    call(onEmailSignInStart),
    call(onCheckUserSession),
    call(onSignOutStart),
    call(onSignUpStart),
    call(onSignUpSuccess),
  ]);
}
