import UserActionTypes from "./user.types.js";

const INITIAL_STATE = {
  currentUser: null,
  claim: false,
};

const userReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case UserActionTypes.SIGN_IN_SUCCESS:
      return {
        ...state,
        currentUser: action.payload,
      };

    case UserActionTypes.SIGN_OUT_SUCCESS:
      return {
        ...state,
        currentUser: null,
        claim: false,
      };

    case UserActionTypes.CLEAR_USER:
      return {
        ...state,
        claim: false,
      };

    case UserActionTypes.SIGN_IN_FAILURE:
    case UserActionTypes.SIGN_OUT_FAILURE:
    case UserActionTypes.SIGN_UP_FAILURE:
    case UserActionTypes.VERIFY_ADMIN_FAILURE:
      return {
        ...state,
        error: action.payload,
        claim: false,
      };

    case UserActionTypes.VERIFY_ADMIN_SUCCESS:
      return {
        ...state,
        error: null,
        claim: true,
      };
    default:
      return state;
  }
};

export default userReducer;
