// combines all of our states / reducers
import { persistReducer } from "redux-persist";
import { combineReducers } from "redux";
// this is telling redux persist to use local storage
// we can also use session
import storage from "redux-persist/lib/storage";

import userReducer from "./user/user.reducer";
import cartReducer from "./cart/cart.reducer";
import productsReducer from "./products/products.reducer";
import categoriesReducer from "./categories/categories.reducer";
import errorReducer from "./error/error.reducer";

const persistConfig = {
  // where to start
  key: "root",
  // where to store
  storage,
  // which reducer will we use
  whitelist: ["cart", "user"],
};

const rootReducer = combineReducers({
  user: userReducer,
  cart: cartReducer,
  allCategory: categoriesReducer,
  allProducts: productsReducer,
  error: errorReducer,
});

// modified version of root reducer with the added functionality of persistance from the
// config declared
export default persistReducer(persistConfig, rootReducer);
