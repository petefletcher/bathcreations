import React from "react";
import { useSelector, useDispatch } from "react-redux";

import Aux from "../hoc/AuxComponent";
import Categories from "../components/Categories";
import "../styles/HomePage.scss";

function HomePage() {
  return (
    <Aux>
      <div className="home-page">
        <section className="home-page__featured">
          <Categories />
        </section>
      </div>
    </Aux>
  );
}

export default HomePage;
