import React, { useState } from "react";
import CheckoutSummary from "../components/basket/CheckoutSummary";

const Checkout = (props) => {
  const handleCancel = () => {
    props.history.goBack();
  };
  const handleComplete = () => props.history.replace("checkout/contact-data");

  return (
    <div>
      <CheckoutSummary />
    </div>
  );
};

export default Checkout;
