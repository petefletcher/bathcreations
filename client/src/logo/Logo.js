import React from "react";
import CustomerLogo from "../img/bc-logo.jpg";
import "../styles/Logo.scss";

const Logo = (props) => {
  return (
    <div
      className="logo"
      style={{ justifyItems: props.jc }}
      onClick={props.clicked}
    >
      <img
        className="logo__image"
        src={CustomerLogo}
        alt="bath-creations-logo"
      />
    </div>
  );
};

export default Logo;
