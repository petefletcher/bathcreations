import firebase from "firebase/app";
// need auth and storage
import "firebase/auth";
import "firebase/firestore";

const config = {
  apiKey: "AIzaSyC7ApHYIb1aAmw5Ic6eWryEldiyNOjq0yI",
  authDomain: "bath-creations.firebaseapp.com",
  databaseURL: "https://bath-creations.firebaseio.com",
  projectId: "bath-creations",
  storageBucket: "bath-creations.appspot.com",
  messagingSenderId: "1072018419929",
  appId: "1:1072018419929:web:f9c62154841640b2384ec8",
  measurementId: "G-R9JH99C8W7",
};

// fn to allow to take the user auth obj from fire base and store in db
export const createUserProfileDocument = async (userAuth, additionalInfo) => {
  // only want to performa this fn if the user auth exists
  if (!userAuth) return;
  // if it does exist, we want to look if its in the document
  // we get two types of obj back from firebase -- query Ref or Query snapshot
  // query -- ask firestore for something
  // we get back a ref of a collection or document
  // fire will always return an obj
  // queryrefenerce represents the current place in the db
  // documentreference we use crud events on a snapshot

  const userRef = firestore.doc(`users/${userAuth.uid}`);

  const snapshot = await userRef.get();

  if (!snapshot.exists) {
    const { displayName, email } = userAuth;
    const createdAt = new Date();

    try {
      await userRef.set({
        displayName,
        email,
        createdAt,
        ...additionalInfo,
      });
    } catch (error) {
      console.error(error);
    }
  }

  //we return the user ref incase we need to use it later in the app
  return userRef;
};

// add json data to firestore
export const addAllProducts = async (productKey, objectsToAdd) => {
  const productsRef = firestore.collection(productKey);

  const batch = firestore.batch();
  Object.keys(objectsToAdd).forEach((obj, i) => {
    const newDocRef = productsRef.doc();

    // we can only do one set at a time so this will batch once loop is complete
    batch.set(newDocRef, objectsToAdd[obj]);
  });

  return await batch.commit();
};

export const convertProductsSnapshotToObject = (products) => {
  const formattedProducts = products.docs.map((doc) => {
    const { category, items, link } = doc.data();

    return {
      id: doc.id,
      category,
      items,
      link,
    };
  });

  return formattedProducts;
};

export const getCurrentUser = () => {
  return new Promise((resolve, reject) => {
    const unsubscribe = auth.onAuthStateChanged((userAuth) => {
      unsubscribe();
      resolve(userAuth);
    }, reject);
  });
};

firebase.initializeApp(config);

export const auth = firebase.auth();
export const firestore = firebase.firestore();

//set up google auth
export const googleProvider = new firebase.auth.GoogleAuthProvider();
// below lane uses the google pop up for auth and singin
googleProvider.setCustomParameters({ prompt: "select_account" });
export const signInWithGoogle = () => auth.signInWithPopup(provider);

export default firebase;
